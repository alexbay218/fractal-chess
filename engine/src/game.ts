import { Ctx, Game, Move } from 'boardgame.io';
import { INVALID_MOVE } from 'boardgame.io/core';
import { currentPlayerIsWhite, executePlayerMove, generatePlayerMoves, generateValidPlayerMoves, initializeGameState, isDraw, isLoss } from './assembly';
import { getBoard, getGameState, getGameStateInfo, getMove, getMoveInfo } from './assembly/convert';
import { isMoveValid } from './assembly/mate';
import { GameStateInfo, MoveInfo } from './types';

interface FCGameState {
  gameState: GameStateInfo,
  moves: MoveInfo[]
}

const movePiece: Move<FCGameState> = (G: FCGameState, ctx: Ctx, moveStr: string) => {
  let inMove: MoveInfo = JSON.parse(moveStr);
  for(let move of G.moves) {
    if(
      getMove(move)[0] === getMove(inMove)[0] &&
      getMove(move)[1] === getMove(inMove)[1]
    ) {
      if(!isMoveValid(getBoard(G.gameState.board), getMove(inMove), currentPlayerIsWhite(getGameState(G.gameState)))) {
        return INVALID_MOVE;
      }
      let newG: FCGameState = {
        gameState: getGameStateInfo(executePlayerMove(getGameState(G.gameState), getMove(JSON.parse(moveStr)))),
        moves: []
      };
      newG.moves = generatePlayerMoves(getGameState(newG.gameState)).map(m => getMoveInfo(m));
      return newG;
    }
  }
  return INVALID_MOVE;
};

export const fractalChessGame: Game<FCGameState> = {
  name: 'fractal-chess',
  setup: (): FCGameState => {
    let g = initializeGameState();
    return {
      gameState: getGameStateInfo(g),
      moves: generatePlayerMoves(g).map(m => getMoveInfo(m))
    };
  },
  moves: {
    movePiece: {
      move: movePiece,
      undoable: false
    }
  },
  turn: {
    moveLimit: 1
  },
  minPlayers: 2,
  maxPlayers: 2,
  endIf: (G: FCGameState, ctx: Ctx) => {
    if(isLoss(getGameState(G.gameState))) {
      return { loser: ctx.currentPlayer };
    }
    if(isDraw(getGameState(G.gameState))) {
      return { draw: true };
    }
  },
  disableUndo: true
};
