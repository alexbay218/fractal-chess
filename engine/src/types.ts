export type Position = u32;
export class PositionInfo {
  recursionLevel: u8;
  recursionCoordinate: u32;
};

export type Direction = u8;

export type Piece = u8;
//Using maps since enums are not very well supported by assemblyscript
let pieceTypes = new Map<u8,string>();
pieceTypes.set(0, 'pawn');
pieceTypes.set(1, 'bishop');
pieceTypes.set(2, 'knight');
pieceTypes.set(3, 'rook');
pieceTypes.set(4, 'queen');
pieceTypes.set(5, 'king');
pieceTypes.set(6, 'phantompawn');
let reversePieceTypes = new Map<string,u8>();
reversePieceTypes.set('pawn', 0);
reversePieceTypes.set('bishop', 1);
reversePieceTypes.set('knight', 2);
reversePieceTypes.set('rook', 3);
reversePieceTypes.set('queen', 4);
reversePieceTypes.set('king', 5);
reversePieceTypes.set('phantompawn', 6);
export function getPieceTypes(): Map<u8, string> {
  return pieceTypes;
}
export function getReversePieceTypes(): Map<string, u8> {
  return reversePieceTypes;
}

export class PieceInfo {
  isWhite: boolean;
  pieceType: string;
};

export type Move = Array<u32>;
export class MoveInfo {
  startingPosition: PositionInfo;
  endingPosition: PositionInfo;
  promotionPiece: PieceInfo;
};

export type MoveHistory = Array<u32>;
export type MoveHistoryInfo = Array<MoveInfo>;

export type Node = u32;
export class NodeInfo {
  isEmpty: boolean;
  isRoot: boolean;
  isPiece: boolean;
  moveRuleCount: u8;
  childIndexOffset: i16;
  parentIndexOffset: i16;
  piece: PieceInfo;
  hasMoved: boolean;
};

export type Board = Array<Node>;
export type BoardInfo = Array<NodeInfo>;

export type BoardHash = u32;
export type BoardHashCount = u32;
export class BoardHashCountInfo {
  count: u8;
  boardHash: BoardHash;
};

export type BoardHashCounts = Array<BoardHashCount>;
export type BoardHashCountsInfo = Array<BoardHashCountInfo>;

export type GameState = Array<Array<u32>>;
/*
GameState[0] = Board
GameState[1] = MoveHistory
GameState[2] = BoardHashCounts
*/
export class GameStateInfo {
  board: BoardInfo;
  moveHistory: MoveHistoryInfo;
  boardHashCounts: BoardHashCountsInfo;
};
