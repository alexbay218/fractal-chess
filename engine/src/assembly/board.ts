import { Board, BoardHashCountInfo, Node, Position, PositionInfo } from '../types';
import { getNode, getNodeInfo, getPieceInfo, getPosition, getPositionInfo } from './convert';
import { nodeChildIndexOffset, nodeHasMoved, nodeIsEmpty, nodeIsPiece, nodeIsRoot, nodeParentIndexOffset, nodePiece } from './node';
import { pieceIsWhite, pieceType } from './piece';
import { positionIntersects, positionRecursionCoordinate, positionRecursionLevel } from './position';

export function initializeBoard(): Board {
  let res: Board = [
    getNode({ isEmpty: true, isRoot: true, isPiece: false, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, piece: { isWhite: false, pieceType: 'pawn' }, hasMoved: false }),
    getNode({ isEmpty: true, isRoot: false, isPiece: false, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, piece: { isWhite: false, pieceType: 'pawn' }, hasMoved: false }),
    getNode({ isEmpty: true, isRoot: false, isPiece: false, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, piece: { isWhite: false, pieceType: 'pawn' }, hasMoved: false }),
    getNode({ isEmpty: true, isRoot: false, isPiece: false, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, piece: { isWhite: false, pieceType: 'pawn' }, hasMoved: false }),
    getNode({ isEmpty: true, isRoot: false, isPiece: false, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, piece: { isWhite: false, pieceType: 'pawn' }, hasMoved: false })
  ];
  //a1 white rook
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: true,
      pieceType: 'rook'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b000000
  }));
  //b1 white knight
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: true,
      pieceType: 'knight'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b010000
  }));
  //c1 white bishop
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: true,
      pieceType: 'bishop'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b000100
  }));
  //d1 white queen
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: true,
      pieceType: 'queen'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b010100
  }));
  //e1 white king
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: true,
      pieceType: 'king'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b000001
  }));
  //f1 white bishop
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: true,
      pieceType: 'bishop'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b010001
  }));
  //g1 white knight
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: true,
      pieceType: 'knight'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b000101
  }));
  //h1 white rook
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: true,
      pieceType: 'rook'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b010101
  }));
  //a2 white pawn
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: true,
      pieceType: 'pawn'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b100000
  }));
  //b2 white pawn
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: true,
      pieceType: 'pawn'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b110000
  }));
  //c2 white pawn
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: true,
      pieceType: 'pawn'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b100100
  }));
  //d2 white pawn
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: true,
      pieceType: 'pawn'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b110100
  }));
  //e2 white pawn
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: true,
      pieceType: 'pawn'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b100001
  }));
  //f2 white pawn
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: true,
      pieceType: 'pawn'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b110001
  }));
  //g2 white pawn
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: true,
      pieceType: 'pawn'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b100101
  }));
  //h2 white pawn
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: true,
      pieceType: 'pawn'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b110101
  }));
  //a8 black rook
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: false,
      pieceType: 'rook'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b101010
  }));
  //b8 black knight
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: false,
      pieceType: 'knight'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b111010
  }));
  //c8 black bishop
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: false,
      pieceType: 'bishop'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b101110
  }));
  //d8 black queen
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: false,
      pieceType: 'queen'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b111110
  }));
  //e8 black king
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: false,
      pieceType: 'king'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b101011
  }));
  //f8 black bishop
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: false,
      pieceType: 'bishop'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b111011
  }));
  //g8 black knight
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: false,
      pieceType: 'knight'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b101111
  }));
  //h8 black rook
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: false,
      pieceType: 'rook'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b111111
  }));
  //a7 black pawn
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: false,
      pieceType: 'pawn'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b001010
  }));
  //b7 black pawn
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: false,
      pieceType: 'pawn'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b011010
  }));
  //c7 black pawn
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: false,
      pieceType: 'pawn'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b001110
  }));
  //d7 black pawn
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: false,
      pieceType: 'pawn'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b011110
  }));
  //e7 black pawn
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: false,
      pieceType: 'pawn'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b001011
  }));
  //f7 black pawn
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: false,
      pieceType: 'pawn'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b011011
  }));
  //g7 black pawn
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: false,
      pieceType: 'pawn'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b001111
  }));
  //h7 black pawn
  res = insertNode(res, getNode({ isEmpty: false, isRoot: false, isPiece: true, moveRuleCount: 0, childIndexOffset: 0, parentIndexOffset: 0, hasMoved: false,
    piece: {
      isWhite: false,
      pieceType: 'pawn'
    }
  }), getPosition({
    recursionLevel: 2,
    recursionCoordinate: 0b011111
  }));
  return res;
}

//Used to move nodes in the array and adjusting parentIndex / childIndex to match
export function moveNode(board: Board, startingIndex: u32, endingIndex: u32): Board {
  const targetNode = board[startingIndex];
  let targetNodeInfo = getNodeInfo(targetNode);
  //Change childIndex of parent node
  const parentIndex = (((startingIndex - 1) - ((startingIndex - 1) % 4)) / 4) + targetNodeInfo.parentIndexOffset;
  let parentNodeInfo = getNodeInfo(board[parentIndex]);
  const firstChildIndex = ((endingIndex - 1) - ((endingIndex - 1) % 4)) + 1;
  parentNodeInfo.childIndexOffset = <i16>((parentIndex * 4) + 1 - firstChildIndex);
  board[parentIndex] = getNode(parentNodeInfo);
  //Change parentIndex of child nodes
  if(!targetNodeInfo.isEmpty && !targetNodeInfo.isPiece) {
    const firstChildIndex = (startingIndex * 4) + 1 - targetNodeInfo.childIndexOffset;
    for(let i = 0;i < 4;i++) {
      const childIndex = firstChildIndex + i;
      let childNodeInfo = getNodeInfo(board[childIndex]);
      childNodeInfo.parentIndexOffset = <i16>(endingIndex - ((((childIndex - 1) - ((childIndex - 1) % 4)) / 4)));
      board[childIndex] = getNode(childNodeInfo);
    }
    targetNodeInfo.childIndexOffset = <i16>((endingIndex * 4) + 1 - firstChildIndex);
  }
  targetNodeInfo.parentIndexOffset = <i16>(parentIndex - ((((endingIndex - 1) - ((endingIndex - 1) % 4)) / 4)));
  board[endingIndex] = getNode(targetNodeInfo);
  return board;
};

//Remove all child nodes and makes current node empty. If parent node needs to be empty, it will remove as needed.
export function removeChildNode(board: Board, position: Position, checkParent: boolean = true): Board {
  const targetRecursionLevel = positionRecursionLevel(position);
  const targetRecursionCoordinate = positionRecursionCoordinate(position);
  //Search through board for position with closest empty / piece node
  let isEnd = false;
  let currentIndex: i32 = (targetRecursionCoordinate & 0x3) + 1;
  let currentRecursionLevel: u8 = 0;
  while(!isEnd) {
    let currentNode = board[currentIndex];
    if((nodeIsEmpty(currentNode) && !nodeIsRoot(currentNode)) || nodeIsPiece(currentNode) || currentRecursionLevel == targetRecursionLevel) {
      isEnd = true;
    }
    else {
      currentRecursionLevel++;
      const targetRecursionCoordinateFragment = (targetRecursionCoordinate >> (2 * currentRecursionLevel)) & 0x3;
      currentIndex = ((currentIndex * 4) - nodeChildIndexOffset(currentNode)) + 1 + targetRecursionCoordinateFragment;
    }
  }
  const closestIndex = currentIndex;
  const closestRecursionLevel: u8 = currentRecursionLevel;
  const closestNode = board[closestIndex];
  //Only proceed if exact node is found
  if(currentRecursionLevel != targetRecursionLevel) {
    return board;
  }
  //Check if child nodes need to get culled
  if(!(nodeIsEmpty(closestNode) && !nodeIsRoot(closestNode)) && !nodeIsPiece(closestNode)) {
    //Recurse through and remove grandchildren
    const firstChildIndex = ((closestIndex * 4) - nodeChildIndexOffset(closestNode)) + 1;
    for(let i = 0;i < 4;i++) {
      board = removeChildNode(board, getPositionFromIndex(board, firstChildIndex + i), false);
    }
    //Actually start removing child nodes
    for(let i = firstChildIndex;i < board.length - 4;i++) {
      board = moveNode(board, i + 4, i);
    }
    for(let i = 0;i < 4;i++) {
      board.pop();
    }
  }
  //Modify closest node to empty node
  board[closestIndex] = getNode({
    isEmpty: true,
    isRoot: false,
    isPiece: false,
    moveRuleCount: 0,
    childIndexOffset: 0,
    parentIndexOffset: nodeParentIndexOffset(closestNode),
    piece: {
      isWhite: false,
      pieceType: 'pawn'
    },
    hasMoved: false
  });
  //Check if parents need to be emptied
  isEnd = false;
  currentIndex = closestIndex;
  while(!isEnd && checkParent) {
    let parentIndex = ((((currentIndex - 1) - ((currentIndex - 1) % 4)) / 4)) + nodeParentIndexOffset(board[currentIndex]);
    let parentNode = board[parentIndex];
    let firstChildIndex = (parentIndex * 4) + 1 - nodeChildIndexOffset(parentNode);
    if(
      !nodeIsRoot(parentNode) &&
      nodeIsEmpty(board[firstChildIndex]) && !nodeIsRoot(board[firstChildIndex]) &&
      nodeIsEmpty(board[firstChildIndex + 1]) && !nodeIsRoot(board[firstChildIndex + 1]) &&
      nodeIsEmpty(board[firstChildIndex + 2]) && !nodeIsRoot(board[firstChildIndex + 2]) &&
      nodeIsEmpty(board[firstChildIndex + 3]) && !nodeIsRoot(board[firstChildIndex + 3])
    ) {
      board = removeChildNode(board, getPositionFromIndex(board, parentIndex), false);
      currentIndex = parentIndex;
    }
    else {
      isEnd = true;
    }
  }
  return board;
}

//Insert node as needed, assumes node is piece type node
export function insertNode(board: Board, node: Node, position: Position): Board {
  const targetRecursionLevel = positionRecursionLevel(position);
  const targetRecursionCoordinate = positionRecursionCoordinate(position);
  //Search through board for closest position for insertion
  let isEnd = false;
  let currentIndex: i32 = (targetRecursionCoordinate & 0x3) + 1;
  let currentRecursionLevel: u8 = 0;
  while(!isEnd) {
    let currentNode = board[currentIndex];
    if((nodeIsEmpty(currentNode) && !nodeIsRoot(currentNode)) || nodeIsPiece(currentNode) || currentRecursionLevel == targetRecursionLevel) {
      isEnd = true;
    }
    else {
      currentRecursionLevel++;
      const targetRecursionCoordinateFragment = (targetRecursionCoordinate >> (2 * currentRecursionLevel)) & 0x3;
      currentIndex = ((currentIndex * 4) - nodeChildIndexOffset(currentNode)) + 1 + targetRecursionCoordinateFragment;
    }
  }
  const closestIndex = currentIndex;
  const closestRecursionLevel: u8 = currentRecursionLevel;
  const closestNode = board[closestIndex];
  //Check if additional parent nodes are needed
  if(targetRecursionLevel > closestRecursionLevel && targetRecursionLevel > 1) {
    board = insertNode(board, getNode({
      isEmpty: true,
      isRoot: false,
      isPiece: false,
      moveRuleCount: 0,
      childIndexOffset: 0,
      parentIndexOffset: 0,
      piece: {
        isWhite: false,
        pieceType: 'pawn'
      },
      hasMoved: false
    }), getPosition({
      recursionLevel: targetRecursionLevel - 1,
      recursionCoordinate: targetRecursionCoordinate
    }));
  }
  //Check if child nodes need to get culled
  else if(!(nodeIsEmpty(closestNode) && !nodeIsRoot(closestNode)) && !nodeIsPiece(closestNode)) {
    board = removeChildNode(board, getPosition({
      recursionLevel: closestRecursionLevel,
      recursionCoordinate: targetRecursionCoordinate
    }));
  }
  //Search through board for parent position
  isEnd = false;
  currentIndex = (targetRecursionCoordinate & 0x3) + 1;
  currentRecursionLevel = 0;
  while(!isEnd) {
    let currentNode = board[currentIndex];
    if(currentRecursionLevel == targetRecursionLevel - 1) {
      isEnd = true;
    }
    else {
      currentRecursionLevel++;
      const targetRecursionCoordinateFragment = (targetRecursionCoordinate >> (2 * currentRecursionLevel)) & 0x3;
      currentIndex = ((currentIndex * 4) - nodeChildIndexOffset(currentNode)) + 1 + targetRecursionCoordinateFragment;
    }
  }
  const parentIndex = currentIndex;
  const parentNode = board[parentIndex];
  let parentNodeInfo = getNodeInfo(board[parentIndex]);
  //Check if parent node is empty and needs child nodes
  if((nodeIsEmpty(parentNode) || nodeIsPiece(parentNode)) && !nodeIsRoot(parentNode)) {
    //Get child index without offset
    let firstChildIndex = (parentIndex * 4) + 1;
    //If child index is larger than the board, then clamp it
    if(firstChildIndex > board.length) {
      firstChildIndex = board.length;
    }
    //Searching for index of any higher recursion level, insertion should happen before
    let foundHigherRL = false;
    for(let i = 1;!foundHigherRL && i < board.length;i++) {
      if(getPositionInfo(getPositionFromIndex(board, i)).recursionLevel > targetRecursionLevel) {
        firstChildIndex = i;
        foundHigherRL = true;
      }
    }
    for(let i = 0;i < 4;i++) {
      //Filling with tmp numbers
      board.push(0);
    }
    for(let i = board.length - 1;i >= firstChildIndex + 4;i--) {
      board = moveNode(board, i - 4, i);
    }
    for(let i = 0;i < 4;i++) {
      board[firstChildIndex + i] = getNode({
        isEmpty: true,
        isRoot: false,
        isPiece: false,
        moveRuleCount: 0,
        childIndexOffset: 0,
        parentIndexOffset: <i16>(parentIndex - ((((firstChildIndex - 1) - ((firstChildIndex - 1) % 4)) / 4))),
        piece: {
          isWhite: false,
          pieceType: 'pawn'
        },
        hasMoved: false
      });
    }
    //Adjust child index of parent
    parentNodeInfo.childIndexOffset = <i16>((parentIndex * 4) + 1 - firstChildIndex);
    parentNodeInfo.isEmpty = false;
    board[parentIndex] = getNode(parentNodeInfo);
  }
  //Search through board for current position
  isEnd = false;
  currentIndex = (targetRecursionCoordinate & 0x3) + 1;
  currentRecursionLevel = 0;
  while(!isEnd) {
    let currentNode = board[currentIndex];
    if(currentRecursionLevel == targetRecursionLevel) {
      isEnd = true;
    }
    else {
      currentRecursionLevel++;
      const targetRecursionCoordinateFragment = (targetRecursionCoordinate >> (2 * currentRecursionLevel)) & 0x3;
      currentIndex = ((currentIndex * 4) - nodeChildIndexOffset(currentNode)) + 1 + targetRecursionCoordinateFragment;
    }
  }
  const targetIndex = currentIndex;
  //Replace current node with target
  let targetNodeInfo = getNodeInfo(node);
  targetNodeInfo.parentIndexOffset = <i16>(parentIndex - ((((targetIndex - 1) - ((targetIndex - 1) % 4)) / 4)));
  board[targetIndex] = getNode(targetNodeInfo);
  return board;
}

export function getPositionFromIndex(board: Board, index: u32): Position {
  let res : PositionInfo = {
    recursionLevel: 0,
    recursionCoordinate: 0
  };
  let isEnd = index == 0;
  let currentIndex = index;
  while(!isEnd) {
    let currentNode = board[currentIndex];
    let parentIndex = (((((currentIndex - 1) - ((currentIndex - 1) % 4)) / 4)) + nodeParentIndexOffset(currentNode));
    let parentNode = board[parentIndex];
    res.recursionLevel++;
    res.recursionCoordinate = (res.recursionCoordinate << 2);
    let childIndexOffset = parentIndex <= 0 ? 0 : nodeChildIndexOffset(parentNode);
    res.recursionCoordinate += ((currentIndex + childIndexOffset - 1 - (parentIndex * 4)) & 0x3);
    currentIndex = parentIndex;
    if(parentIndex <= 0) {
      isEnd = true;
    }
  }
  res.recursionLevel--;
  return getPosition(res);
}

export function getIndexFromPosition(board: Board, position: Position): i32 {
  const targetRecursionLevel = positionRecursionLevel(position);
  const targetRecursionCoordinate = positionRecursionCoordinate(position);
  //Search through board for position with closest empty / piece node
  let currentIndex: i32 = (targetRecursionCoordinate & 0x3) + 1;
  let currentRecursionLevel: u8 = 0;
  while(true) {
    let currentNode = board[currentIndex];
    if((nodeIsEmpty(currentNode) && !nodeIsRoot(currentNode)) || nodeIsPiece(currentNode) || currentRecursionLevel == targetRecursionLevel) {
      return currentIndex;
    }
    else {
      currentRecursionLevel++;
      const targetRecursionCoordinateFragment = (targetRecursionCoordinate >> (2 * currentRecursionLevel)) & 0x3;
      currentIndex = ((currentIndex * 4) - nodeChildIndexOffset(currentNode)) + 1 + targetRecursionCoordinateFragment;
    }
  }
}

export function getNodeFromPosition(board: Board, position: Position): Node {
  const targetRecursionLevel = positionRecursionLevel(position);
  const targetRecursionCoordinate = positionRecursionCoordinate(position);
  //Search through board for position with closest empty / piece node
  let currentIndex: i32 = (targetRecursionCoordinate & 0x3) + 1;
  let currentRecursionLevel: u8 = 0;
  while(true) {
    let currentNode = board[currentIndex];
    if((nodeIsEmpty(currentNode) && !nodeIsRoot(currentNode)) || nodeIsPiece(currentNode) || currentRecursionLevel == targetRecursionLevel) {
      return currentNode;
    }
    else {
      currentRecursionLevel++;
      const targetRecursionCoordinateFragment = (targetRecursionCoordinate >> (2 * currentRecursionLevel)) & 0x3;
      currentIndex = ((currentIndex * 4) - nodeChildIndexOffset(currentNode)) + 1 + targetRecursionCoordinateFragment;
    }
  }
}

export function splitNode(board: Board, position: Position): Board {
  let targetNode = getNodeFromPosition(board, position);
  const targetRecursionLevel = positionRecursionLevel(position);
  const targetRecursionCoordinate = positionRecursionCoordinate(position);
  for(let i = 0;i < 4;i++) {
    let currRecursionCoordinate = targetRecursionCoordinate + (i << ((targetRecursionLevel + 1) * 2));
    let currPosition = getPosition({
      recursionLevel: targetRecursionLevel + 1,
      recursionCoordinate: currRecursionCoordinate
    });
    board = insertNode(board, targetNode, currPosition);
  }
  return board;
}

export function mergeNodes(board: Board): Board {
  let didExecute = false;
  for(let i = 1;!didExecute && i <= board.length - 4;i += 4) {
    let currNode = board[i];
    let isSame = nodeIsPiece(currNode);
    let hasMoved = nodeHasMoved(currNode);
    for(let j = 0;j < 4;j++) {
      if(
        !nodeIsPiece(board[i + j]) ||
        nodePiece(board[i + j]) != nodePiece(currNode)
      ) {
        isSame = false;
      }
      if(!hasMoved && nodeHasMoved(board[i + j])) {
        hasMoved = true;
      }
    }
    if(isSame) {
      let currPosition = getPositionFromIndex(board, i);
      let parentPosition = getPosition({
        recursionLevel: positionRecursionLevel(currPosition) - 1,
        recursionCoordinate: positionRecursionCoordinate(currPosition)
      });
      if(!nodeHasMoved(currNode) && hasMoved) {
        //Change node to has moved if it isn't already
        currNode = getNode({
          isEmpty: false,
          isRoot: false,
          isPiece: true,
          moveRuleCount: 0,
          childIndexOffset: 0,
          parentIndexOffset: nodeParentIndexOffset(currNode),
          piece: getPieceInfo(nodePiece(currNode)),
          hasMoved: true
        });
      }
      board = removeChildNode(board, parentPosition, false);
      board = insertNode(board, currNode, parentPosition);
      didExecute = true;
    }
  }
  if(didExecute) {
    board = mergeNodes(board);
  }
  return board;
}

export function wipePhantomPawns(board: Board): Board {
  let phantomPawnList: Position[] = [];
  for(let i = 0;i < board.length;i++) {
    let currNode = board[i];
    if(nodeIsPiece(currNode)) {
      let currPiece = nodePiece(currNode);
      if(pieceType(currPiece) == 'phantompawn') {
        phantomPawnList.push(getPositionFromIndex(board, i));
      }
    }
  }
  for(let i = 0;i < phantomPawnList.length;i++) {
    board = removeChildNode(board, phantomPawnList[i]);
  }
  return board;
}

export function getPhantomPawnPosition(board: Board, position: Position): Position {
  for(let i = 0;i < board.length;i++) {
    let currNode = board[i];
    if(nodeIsPiece(currNode)) {
      let currPiece = nodePiece(currNode);
      if(pieceType(currPiece) == 'phantompawn') {
        let currPosition = getPositionFromIndex(board, i);
        if(positionIntersects(currPosition, position)) {
          return currPosition;
        }
      }
    }
  }
  return 0;
}

export function positionHasPiece(board: Board, position: Position): boolean {
  for(let i = 0;i < board.length;i++) {
    let currNode = board[i];
    if(nodeIsPiece(currNode)) {
      let currPosition = getPositionFromIndex(board, i);
      if(positionIntersects(currPosition, position)) {
        return true;
      }
    }
  }
  return false;
}