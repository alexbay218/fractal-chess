import { Board, BoardHashCounts, Move } from '../types';
import { getBoardHashCountInfo } from './convert';
import { executeMove, generateMoves, generateValidMoves, hasValidMoves } from './move';
import { nodeIsPiece, nodeMoveRuleCount, nodePiece } from './node';
import { pieceIsWhite, pieceType } from './piece';

function kingCount(board: Board, isWhite: boolean): u32 {
  let kingCount = 0;
  for(let i = 0;i < board.length;i++) {
    let currNode = board[i];
    if(nodeIsPiece(currNode)) {
      let currPiece = nodePiece(currNode);
      if(pieceType(currPiece) == 'king' && pieceIsWhite(currPiece) == isWhite) {
        kingCount++;
      }
    }
  }
  return kingCount;
}

export function isCheck(board: Board, isWhite: boolean): boolean {
  let oppositeMoves = generateMoves(board.slice(0), !isWhite, true, true);
  let originalKingCount = kingCount(board, isWhite);
  for(let i = 0;i < oppositeMoves.length;i++) {
    let currMove = oppositeMoves[i];
    let newBoard = executeMove(board.slice(0), currMove);
    let newKingCount = kingCount(newBoard, isWhite);
    if(newKingCount < originalKingCount) {
      return true;
    }
  }
  return false;
}

export function isMoveValid(board: Board, move: Move, isWhite: boolean): boolean {
  let newBoard = executeMove(board.slice(0), move);
  return !isCheck(newBoard, isWhite);
}

export function isCheckmate(board: Board, isWhite: boolean): boolean {
  return isCheck(board, isWhite) && !hasValidMoves(board, isWhite);
}

export function isStalemate(board: Board, isWhite: boolean): boolean {
  return !isCheck(board, isWhite) && !hasValidMoves(board, isWhite);
}

export function isRepetitionDraw(boardHashCounts: BoardHashCounts): boolean {
  for(let i = 0;i < boardHashCounts.length;i++) {
    if(getBoardHashCountInfo(boardHashCounts[i]).count >= 3) {
      return true;
    }
  }
  return false;
}

export function isMoveCountDraw(board: Board): boolean {
  return nodeMoveRuleCount(board[0]) >= 50;
}