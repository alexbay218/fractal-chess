import { Board, getPieceTypes, Move, Piece, Position } from '../types';
import { getNodeFromPosition, getPositionFromIndex } from './board';
import { getMove, getPiece, getPieceInfo, getPosition, getPositionInfo } from './convert';
import { isCheck, isMoveValid } from './mate';
import { createMove, moveEndPosition, moveStartPosition } from './move';
import { nodeHasMoved, nodeIsPiece, nodePiece } from './node';
import { positionIntersects, positionIsBackRow, positionRecursionCoordinate, positionStep } from './position';

const vectors: u8[][] = [
  //Normal vectors
  [0],
  [0,1],
  [1],
  [1,2],
  [2],
  [2,3],
  [3],
  [3,0],
  //King single position
  [0],
  [0,1],
  [1],
  [1,2],
  [2],
  [2,3],
  [3],
  [3,0],
  //Knight
  [0,0,1],
  [0,0,3],
  [1,1,0],
  [1,1,2],
  [2,2,1],
  [2,2,3],
  [3,3,2],
  [3,3,0]
];
const isVector: boolean[] = [
  true,
  true,
  true,
  true,
  true,
  true,
  true,
  true,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false
];
const vectorPieces: string[][] = [
  ['rook','queen'],
  ['bishop','queen'],
  ['rook','queen'],
  ['bishop','queen'],
  ['rook','queen'],
  ['bishop','queen'],
  ['rook','queen'],
  ['bishop','queen'],
  ['king'],
  ['king'],
  ['king'],
  ['king'],
  ['king'],
  ['king'],
  ['king'],
  ['king'],
  ['knight'],
  ['knight'],
  ['knight'],
  ['knight'],
  ['knight'],
  ['knight'],
  ['knight'],
  ['knight']
];
const promotablePiece: string[] = [
  'knight',
  'bishop',
  'rook',
  'queen'
];

export function pieceType(piece: Piece): string {
  return getPieceTypes().get((piece >> 1 & 0x7)) as string
}

export function pieceIsWhite(piece: Piece): boolean {
  return (piece & 0x1) == 0;
}

export function generatePieceMoves(board: Board, position: Position, skipCastlingCheck: boolean = false): Move[] {
  let targetNode = getNodeFromPosition(board, position);
  let moveList: Move[] = [];
  if(nodeIsPiece(targetNode)) {
    let targetPiece = nodePiece(targetNode);
    let playerIsWhite = pieceIsWhite(targetPiece);
    //Vector / Position based move generation
    for(let i = 0;i < vectors.length;i++) {
      let currVector = vectors[i];
      let currVectorPiece = vectorPieces[i];
      let currPosition = position;
      let isEnd = false;
      while(!isEnd && currVectorPiece.includes(pieceType(targetPiece))) {
        let newPosition = currPosition;
        //Executing vector / position stepping operation
        //If any of the stepping crosses boundary, should stop
        for(let j = 0;!isEnd && j < currVector.length;j++) {
          let tmpPosition = positionStep(newPosition, currVector[j]);
          if(positionRecursionCoordinate(newPosition) == positionRecursionCoordinate(tmpPosition)) {
            isEnd = true;
          }
          newPosition = tmpPosition;
        }
        if(!isEnd) {
          //Checking for collisions (ignore phantompawn)
          let isColliding = false;
          let sameColor = false;
          for(let j = 0;!isEnd && j < board.length;j++) {
            if(nodeIsPiece(board[j])) {
              let testPiece = nodePiece(board[j]);
              let testPosition = getPositionFromIndex(board, j);
              if(position != testPosition && pieceType(testPiece) != 'phantompawn') {
                if(positionIntersects(newPosition, testPosition)) {
                  isColliding = true;
                  if(pieceIsWhite(targetPiece) == pieceIsWhite(testPiece)) {
                    sameColor = true;
                  }
                }
              }
            }
          }
          //Allow move if not colliding with same color pieces
          if(!sameColor) {
            moveList.push(createMove(position, newPosition, targetPiece));
          }
          //Stop vector if move is colliding
          if(isColliding) { 
            isEnd = true;
          }
        }
        currPosition = newPosition;
        //If not vector, execute only once
        if(!isVector[i]) {
          isEnd = true;
        }
      }
    }
    //Pawn specific moves
    if(pieceType(targetPiece) == 'pawn') {
      let direction: u8 = playerIsWhite ? 0 : 2;
      let forwardStepPosition = positionStep(position, direction);
      let eastStepPosition = positionStep(forwardStepPosition, 1);
      let westStepPosition = positionStep(forwardStepPosition, 3);
      let forward2StepPosition = positionStep(forwardStepPosition, direction);
      let canMoveForward = true;
      let canMoveEast = false;
      let canMoveWest = false;
      let canMoveForward2 = !nodeHasMoved(targetNode);
      for(let j = 0;j < board.length;j++) {
        if(nodeIsPiece(board[j])) {
          let testPiece = nodePiece(board[j]);
          let testPosition = getPositionFromIndex(board, j);
          let pieceEqual = pieceIsWhite(targetPiece) == pieceIsWhite(testPiece);
          if(position != testPosition) {
            if(canMoveForward && positionIntersects(forwardStepPosition, testPosition)) {
              canMoveForward = false;
            }
            if(!canMoveEast && positionIntersects(eastStepPosition, testPosition) && !pieceEqual) {
              if(
                forwardStepPosition != position &&
                forwardStepPosition != eastStepPosition
              ) {
                canMoveEast = true;
              }
            }
            if(!canMoveWest && positionIntersects(westStepPosition, testPosition) && !pieceEqual) {
              if(
                forwardStepPosition != position &&
                forwardStepPosition != westStepPosition
              ) {
                canMoveWest = true;
              }
            }
            if(canMoveForward2 && positionIntersects(forward2StepPosition, testPosition)) {
              canMoveForward2 = false;
            }
          }
        }
      }
      if(canMoveForward) {
        //Check if promotion type move
        if(positionIsBackRow(forwardStepPosition, playerIsWhite)) {
          for(let k = 0;k < promotablePiece.length;k++) {
            moveList.push(createMove(position, forwardStepPosition, getPiece({
              isWhite: playerIsWhite,
              pieceType: promotablePiece[k]
            })));
          }
        }
        else {
          moveList.push(createMove(position, forwardStepPosition, targetPiece));
        }
      }
      if(canMoveForward && canMoveForward2) {
        moveList.push(createMove(position, forward2StepPosition, targetPiece));
      }
      if(canMoveEast) {
        moveList.push(createMove(position, eastStepPosition, targetPiece));
      }
      if(canMoveWest) {
        moveList.push(createMove(position, westStepPosition, targetPiece));
      }
    }
    //Castling specific moves
    if(pieceType(targetPiece) == 'king' && !nodeHasMoved(targetNode)) {
      //Look for left rook
      let leftRookPosition = playerIsWhite ? getPosition({
        recursionLevel: 2,
        recursionCoordinate: 0b000000
      }) : getPosition({
        recursionLevel: 2,
        recursionCoordinate: 0b101010
      });
      let leftRookNode = getNodeFromPosition(board, leftRookPosition);
      let leftRookPiece = nodePiece(leftRookNode);

      //Look for right rook
      let rightRookPosition = playerIsWhite ? getPosition({
        recursionLevel: 2,
        recursionCoordinate: 0b010101
      }) : getPosition({
        recursionLevel: 2,
        recursionCoordinate: 0b111111
      });
      let rightRookNode = getNodeFromPosition(board, rightRookPosition);
      let rightRookPiece = nodePiece(rightRookNode);

      //Look at unmoved king
      let kingPosition = playerIsWhite ? getPosition({
        recursionLevel: 2,
        recursionCoordinate: 0b000001
      }) : getPosition({
        recursionLevel: 2,
        recursionCoordinate: 0b101011
      });
      let leftStepPosition = positionStep(kingPosition, 3);
      let left2StepPosition = positionStep(leftStepPosition, 3);
      let rightStepPosition = positionStep(kingPosition, 1);
      let right2StepPosition = positionStep(rightStepPosition, 1);

      //Check that king is not in check
      if(position == kingPosition && !skipCastlingCheck && !isCheck(board, playerIsWhite)) {
        if(pieceType(leftRookPiece) == 'rook' && !nodeHasMoved(leftRookNode)) {
          //Search for move that indicates rook can move to leftStepPosition
          let leftSideClear = false;
          let leftRookMoveList = generatePieceMoves(board.slice(0), leftRookPosition, true);
          for(let i = 0;i < leftRookMoveList.length;i++) {
            let currMove = leftRookMoveList[i];
            if(moveStartPosition(currMove) == leftRookPosition && moveEndPosition(currMove) == leftStepPosition) {
              leftSideClear = true;
            }
          }

          //Check that both left stepping moves are valid
          if(leftSideClear && isMoveValid(board, createMove(position, leftStepPosition, targetPiece), playerIsWhite)) {
            let leftCastlingMove = createMove(position, left2StepPosition, targetPiece);
            if(isMoveValid(board, leftCastlingMove, playerIsWhite)) {
              moveList.push(leftCastlingMove);
            }
          }
        }
        if(pieceType(rightRookPiece) == 'rook' && !nodeHasMoved(rightRookNode)) {
          //Search for move that indicates rook can move to rightStepPosition
          let rightSideClear = false;
          let rightRookMoveList = generatePieceMoves(board.slice(0), rightRookPosition, true);
          for(let i = 0;i < rightRookMoveList.length;i++) {
            let currMove = rightRookMoveList[i];
            if(moveStartPosition(currMove) == rightRookPosition && moveEndPosition(currMove) == rightStepPosition) {
              rightSideClear = true;
            }
          }

          //Check that both right stepping moves are valid
          if(rightSideClear && isMoveValid(board, createMove(position, rightStepPosition, targetPiece), playerIsWhite)) {
            let rightCastlingMove = createMove(position, right2StepPosition, targetPiece);
            if(isMoveValid(board, rightCastlingMove, playerIsWhite)) {
              moveList.push(rightCastlingMove);
            }
          }
        }
      }
    }
  }
  return moveList;
}