import { Board, BoardHash, BoardHashCount, BoardHashCountInfo, BoardHashCounts, BoardHashCountsInfo, BoardInfo, GameState, GameStateInfo, Move, MoveHistory, MoveHistoryInfo, MoveInfo, Node, NodeInfo, Piece, PieceInfo, getPieceTypes, Position, PositionInfo, getReversePieceTypes } from '../types';
import { hashBoard } from './hash';
import { createMove, moveEndPosition, movePromotionPiece, moveStartPosition } from './move';
import { nodeChildIndexOffset, nodeHasMoved, nodeIsEmpty, nodeIsPiece, nodeIsRoot, nodeMoveRuleCount, nodeParentIndexOffset, nodePiece } from './node';
import { pieceIsWhite, pieceType } from './piece';
import { positionRecursionCoordinate, positionRecursionLevel } from './position';

export function getPosition(positionInfo: PositionInfo): Position {
  return (
    (positionInfo.recursionLevel & 0xf) +
    ((positionInfo.recursionCoordinate & 0x3ffffff) << 4)
  );
}

export function getPositionInfo(position: Position): PositionInfo {
  return {
    recursionLevel: positionRecursionLevel(position),
    recursionCoordinate: positionRecursionCoordinate(position)
  };
}

export function getPiece(pieceInfo: PieceInfo): Piece {
  return (
    (pieceInfo.isWhite ? 0 : 1) +
    ((getReversePieceTypes().get(pieceInfo.pieceType) & 0x7) << 1)
  );
}

export function getPieceInfo(piece: Piece): PieceInfo {
  return {
    isWhite: pieceIsWhite(piece),
    pieceType: pieceType(piece)
  };
}

export function getMove(moveInfo: MoveInfo): Move {
  return createMove(
    getPosition(moveInfo.startingPosition),
    getPosition(moveInfo.endingPosition),
    getPiece(moveInfo.promotionPiece)
  );
}

export function getMoveInfo(move: Move): MoveInfo {
  return {
    startingPosition: getPositionInfo(moveStartPosition(move)),
    endingPosition: getPositionInfo(moveEndPosition(move)),
    promotionPiece: getPieceInfo(movePromotionPiece(move))
  };
}

export function getMoveHistory(moveHistoryInfo: MoveHistoryInfo): MoveHistory {
  return moveHistoryInfo.map<Move>(moveInfo => getMove(moveInfo)).flat();
}

export function getMoveHistoryInfo(moveHistory: MoveHistory): MoveHistoryInfo {
  let res = [];
  for(let i = 0;i < moveHistory.length;i+=2) {
    if(i + 1 < moveHistory.length) {
      res.push(getMoveInfo([moveHistory[i], moveHistory[i + 1]]));
    }
  }
  return res;
}

export function getNode(nodeInfo: NodeInfo): Node {
  if(nodeInfo.isEmpty) {
    if(nodeInfo.isRoot) {
      return (
        (1 << 1) +
        ((nodeInfo.moveRuleCount & 0xff) << 2)
      );
    }
    else {
      return (
        (<u32>(nodeInfo.parentIndexOffset & 0x7fff) << 17)
      );
    }
  }
  if(nodeInfo.isPiece) {
    return (
      1 +
      (1 << 1) +
      ((getPiece(nodeInfo.piece) & 0xf) << 2) +
      ((nodeInfo.hasMoved ? 1 : 0) << 6) +
      (<u32>(nodeInfo.parentIndexOffset & 0x7fff) << 17)
    );
  }
  return (
    1 +
    (<u32>(nodeInfo.childIndexOffset & 0x7fff) << 2) +
    (<u32>(nodeInfo.parentIndexOffset & 0x7fff) << 17)
  );
}

export function getNodeInfo(node: Node): NodeInfo {
  let res: NodeInfo = {
    isEmpty: false,
    isRoot: false,
    isPiece: false,
    moveRuleCount: 0,
    childIndexOffset: 0,
    parentIndexOffset: 0,
    piece: {
      isWhite: false,
      pieceType: 'pawn'
    },
    hasMoved: false
  };
  //isEmpty
  if(nodeIsEmpty(node)) {
    res.isEmpty = true;
    //isRoot
    if(nodeIsRoot(node)) {
      res.isRoot = true;
      res.moveRuleCount = nodeMoveRuleCount(node);
    }
    else {
      res.parentIndexOffset = nodeParentIndexOffset(node);
    }
  }
  else {
    //isPiece
    if(nodeIsPiece(node)) {
      res.isPiece = true;
      res.piece = getPieceInfo(nodePiece(node));
      res.hasMoved = nodeHasMoved(node);
    }
    else {
      res.childIndexOffset = nodeChildIndexOffset(node);
    }
    res.parentIndexOffset = nodeParentIndexOffset(node);
  }
  return res;
}

export function getBoard(boardInfo: BoardInfo): Board {
  return boardInfo.map<Node>(nodeInfo => getNode(nodeInfo));
}

export function getBoardInfo(board: Board): BoardInfo {
  return board.map<NodeInfo>(node => getNodeInfo(node));
}

export function getBoardHash(board: Board): BoardHash {
  return hashBoard(board);
}

export function getBoardHashCount(boardHashCountInfo: BoardHashCountInfo): BoardHashCount {
  return (
    (boardHashCountInfo.count & 0x3) +
    ((boardHashCountInfo.boardHash & 0x3fffffff) << 2)
  );
}

export function getBoardHashCountInfo(boardHashCount: BoardHashCount): BoardHashCountInfo {
  return {
    count: <u8>(boardHashCount & 0x3),
    boardHash: <u16>((boardHashCount >> 2) & 0x3fffffff)
  };
}

export function getBoardHashCounts(boardHashCountsInfo: BoardHashCountsInfo): BoardHashCounts {
  return boardHashCountsInfo.map<BoardHashCount>(boardHashCountInfo => getBoardHashCount(boardHashCountInfo));
}

export function getBoardHashCountsInfo(boardHashCounts: BoardHashCounts): BoardHashCountsInfo {
  return boardHashCounts.map<BoardHashCountInfo>(boardHashCount => getBoardHashCountInfo(boardHashCount));
}

export function getGameState(gameStateInfo: GameStateInfo): GameState {
  return [
    getBoard(gameStateInfo.board),
    getMoveHistory(gameStateInfo.moveHistory),
    getBoardHashCounts(gameStateInfo.boardHashCounts)
  ];
}

export function getGameStateInfo(gameState: GameState): GameStateInfo {
  return {
    board: getBoardInfo(gameState[0]),
    moveHistory: getMoveHistoryInfo(gameState[1]),
    boardHashCounts: getBoardHashCountsInfo(gameState[2])
  };
}