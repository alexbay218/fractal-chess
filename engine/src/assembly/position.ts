import { Direction, Position } from '../types';
import { getPosition } from './convert';

export function positionRecursionLevel(position: Position): u8 {
  return <u8>(position & 0xf);
}

export function positionRecursionCoordinate(position: Position): u32 {
  return ((position >> 4) & 0x3ffffff);
}

export function positionIntersects(position1: Position, position2: Position): boolean {
  let minRecursionLevel: u8 = positionRecursionLevel(position1);
  if(positionRecursionLevel(position2) < minRecursionLevel) {
    minRecursionLevel = positionRecursionLevel(position2);
  }
  return (positionRecursionCoordinate(position1) & ~<u32>(0xffffffff << ((minRecursionLevel + 1) * 2))) == (positionRecursionCoordinate(position2) & ~<u32>(0xffffffff << ((minRecursionLevel + 1) * 2)));
}

export function positionIsBackRow(position: Position, isWhite: boolean): boolean {
  let targetRecursionLevel = positionRecursionLevel(position);
  let targetRecursionCoordinate = positionRecursionCoordinate(position);
  let verticalCoordinate = 0;
  for(let i = 0;i <= <i32>targetRecursionLevel;i++) {
    verticalCoordinate += (targetRecursionCoordinate >> (i * 2 + 1) & 1) << (targetRecursionLevel - i);
  }
  return isWhite ? verticalCoordinate == (~(0xffffffff << (targetRecursionLevel + 1))) : verticalCoordinate == 0;
}

export function positionStep(position: Position, direction: Direction): Position {
  let targetRecursionLevel = positionRecursionLevel(position);
  let targetRecursionCoordinate = positionRecursionCoordinate(position);
  let isVertical = direction == 0 || direction == 2;
  let isPositive = direction == 0 || direction == 1;
  let horizontalCoordinate = 0;
  let verticalCoordinate = 0;
  for(let i = 0;i <= <i32>targetRecursionLevel;i++) {
    horizontalCoordinate += (targetRecursionCoordinate >> (i * 2) & 1) << (targetRecursionLevel - i);
    verticalCoordinate += (targetRecursionCoordinate >> (i * 2 + 1) & 1) << (targetRecursionLevel - i);
  }
  if(isVertical) {
    if(!(verticalCoordinate == 0 && !isPositive)) {
      verticalCoordinate += isPositive ? 1 : -1;
    }
    //No change if overflow
    if(verticalCoordinate != (verticalCoordinate & (0x1fff >> (12 - targetRecursionLevel)))) {
      return position;
    }
  }
  else {
    if(!(horizontalCoordinate == 0 && !isPositive)) {
      horizontalCoordinate += isPositive ? 1 : -1;
    }
    //No change if overflow
    if(horizontalCoordinate != (horizontalCoordinate & (0x1fff >> (12 - targetRecursionLevel)))) {
      return position;
    }
  }
  let newRecursionCoordinate = 0;
  for(let i = 0;i <= <i32>targetRecursionLevel;i++) {
    newRecursionCoordinate += ((horizontalCoordinate & (1 << (targetRecursionLevel - i))) >> (targetRecursionLevel - i)) << (i * 2);
    newRecursionCoordinate += ((verticalCoordinate & (1 << (targetRecursionLevel - i))) >> (targetRecursionLevel - i)) << (i * 2 + 1);
  }
  return getPosition({
    recursionLevel: targetRecursionLevel,
    recursionCoordinate: newRecursionCoordinate
  });
}