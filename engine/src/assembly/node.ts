import { Node } from '../types';

export function nodeIsEmpty(node: Node): boolean {
  return (node & 0x1) == 0;
}

export function nodeIsRoot(node: Node): boolean {
  return nodeIsEmpty(node) && ((node >> 1) & 0x1) == 1;
}

export function nodeIsPiece(node: Node): boolean {
  return !nodeIsEmpty(node) && ((node >> 1) & 0x1) == 1;
}

export function nodeMoveRuleCount(node: Node): u8 {
  return <u8>((node >> 2) & 0xff);
}

export function setNodeMoveRuleCount(node: Node, count: u8): Node {
  return <u32>((count & 0xff) << 2) + (node & 0x3);
}

export function nodeChildIndexOffset(node: Node): i16 {
  let ret = <u16>((node >> 2) & 0x7fff);
  if(ret >= 0x4000) {
    return <i16>(-(~ret + 0x8001));
  }
  return <i16>ret;
}

export function nodeParentIndexOffset(node: Node): i16 {
  let ret = <u16>((node >> 17) & 0x7fff);
  if(ret >= 0x4000) {
    return <i16>(-(~ret + 0x8001));
  }
  return <i16>ret;
}

export function nodePiece(node: Node): u8 {
  return <u8>((node >> 2) & 0xf);
}

export function nodeHasMoved(node: Node): boolean {
  return ((node >> 6) & 0x1) == 1;
}
