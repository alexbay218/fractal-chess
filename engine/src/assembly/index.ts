import { GameState, Move } from '../types';
import { initializeBoard } from './board';
import { addHash } from './hash';
import { isCheckmate, isMoveCountDraw, isRepetitionDraw, isStalemate } from './mate';
import { executeMove, generateMoves, generateValidMoves } from './move';

export function initializeGameState(): GameState {
  return [
    initializeBoard(),
    [],
    []
  ];
}

export function copyGameState(gameState: GameState): GameState {
  return [
    gameState[0].slice(0),
    gameState[1].slice(0),
    gameState[2].slice(0)
  ];
}

export function executePlayerMove(gameState: GameState, move: Move): GameState {
  gameState[0] = executeMove(gameState[0], move);
  gameState[1].push(move[0]);
  gameState[1].push(move[1]);
  gameState[2] = addHash(gameState[2], gameState[0]);
  return [
    gameState[0],
    gameState[1],
    gameState[2]
  ];
}

export function currentPlayerIsWhite(gameState: GameState): boolean {
  return gameState[1].length % 4 == 0;
}

export function oppositePlayerIsWhite(gameState: GameState): boolean {
  return gameState[1].length % 4 != 0;
}

export function generatePlayerMoves(gameState: GameState): Move[] {
  return generateMoves(gameState[0], currentPlayerIsWhite(gameState));
}

export function generateValidPlayerMoves(gameState: GameState): Move[] {
  return generateValidMoves(gameState[0], currentPlayerIsWhite(gameState));
}

export function isLoss(gameState: GameState): boolean {
  return isCheckmate(gameState[0], currentPlayerIsWhite(gameState));
}

export function isDraw(gameState: GameState): boolean {
  return isMoveCountDraw(gameState[0]) ||
  isRepetitionDraw(gameState[2]) ||
  isStalemate(gameState[0], currentPlayerIsWhite(gameState));
}