import { Board, Move, Piece, Position } from '../types';
import { getIndexFromPosition, getNodeFromPosition, getPhantomPawnPosition, getPositionFromIndex, insertNode, mergeNodes, positionHasPiece, removeChildNode, splitNode, wipePhantomPawns } from './board';
import { getNode, getPieceInfo, getPosition } from './convert';
import { isMoveValid } from './mate';
import { nodeHasMoved, nodeIsPiece, nodeMoveRuleCount, nodeParentIndexOffset, nodePiece, setNodeMoveRuleCount } from './node';
import { generatePieceMoves, pieceIsWhite, pieceType } from './piece';
import { positionRecursionLevel, positionStep } from './position';

export function createMove(startingPosition: Position, endingPosition: Position, promotionPiece: Piece): Move {
  return [
    (
      startingPosition +
      (<u32>(promotionPiece & 0x3) << 30)
    ),
    (
      endingPosition +
      (<u32>(promotionPiece & 0xc) << 28)
    )
  ];
}

export function moveStartPosition(move: Move): Position {
  return move[0] & 0x3fffffff;
}

export function moveEndPosition(move: Move): Position {
  return move[1] & 0x3fffffff;
}

export function movePromotionPiece(move: Move): Piece {
  return <u8>((move[0] >> 30) & 0x3) + <u8>((move[1] >> 28) & 0xc);
}

export function executeMove(board: Board, move: Move): Board {
  let startPosition = moveStartPosition(move);
  let startIndex = getIndexFromPosition(board, startPosition);
  let realStartPosition = getPositionFromIndex(board, startIndex);
  if(positionRecursionLevel(startPosition) == positionRecursionLevel(realStartPosition) + 1) {
    board = splitNode(board, realStartPosition);
  }
  let endPosition = moveEndPosition(move);
  let endIndex = getIndexFromPosition(board, endPosition);
  let realEndPosition = getPositionFromIndex(board, endIndex);
  let startNode = getNodeFromPosition(board, startPosition);
  let endNode = getNodeFromPosition(board, endPosition);
  //Stop if start position has no pieces
  if(!nodeIsPiece(startNode)) {
    return board;
  }
  //Move rule-checks
  let isPawnMove = false;
  let isCapture = positionHasPiece(board, endPosition);
  //Check if phantom pawn spawning is needed (for en passant)
  let startPiece = nodePiece(startNode);
  let playerIsWhite = pieceIsWhite(startPiece);
  let direction: u8 = playerIsWhite ? 0 : 2;
  let forwardPosition = positionStep(startPosition, direction);
  let forward2Position = positionStep(forwardPosition, direction);
  let spawnPhantomPawn = false;
  if(pieceType(startPiece) == 'pawn') {
    isPawnMove = true;
    if(
      startPosition != forwardPosition &&
      forwardPosition != forward2Position &&
      endPosition == forward2Position
    ) {
      spawnPhantomPawn = true;
    }
  }
  //Check if pawn killing is needed (from en passant)
  let oppositeDirection: u8 = !playerIsWhite ? 0 : 2;
  let phantomPawnPosition = getPhantomPawnPosition(board, endPosition);
  let phantomPawnNode = getNodeFromPosition(board, phantomPawnPosition);
  let oppositeForwardPosition = positionStep(phantomPawnPosition, oppositeDirection);
  let oppositeForwardIndex = getIndexFromPosition(board, oppositeForwardPosition);
  let realOppositeForwardPosition = getPositionFromIndex(board, oppositeForwardIndex);
  let endPieceIsPhantomPawn = false;
  if(nodeIsPiece(phantomPawnNode)) {
    let endPiece = nodePiece(phantomPawnNode);
    if(
      pieceType(endPiece) == 'phantompawn' &&
      endPosition != oppositeForwardPosition
    ) {
      endPieceIsPhantomPawn = true;
    }
  }
  //Check if additional rook move is needed (from castling)
  let leftStepPosition = positionStep(startPosition, 3);
  let left2StepPosition = positionStep(leftStepPosition, 3);
  let leftCastling = false;
  let rightStepPosition = positionStep(startPosition, 1);
  let right2StepPosition = positionStep(rightStepPosition, 1);
  let rightCastling = false;
  if(
    pieceType(startPiece) == 'king' &&
    startPosition != leftStepPosition &&
    leftStepPosition != left2StepPosition &&
    endPosition == left2StepPosition
  ) {
    leftCastling = true;
  }
  if(
    pieceType(startPiece) == 'king' &&
    startPosition != rightStepPosition &&
    rightStepPosition != right2StepPosition &&
    endPosition == right2StepPosition
  ) {
    rightCastling = true;
  }
  //Change node to has moved if it isn't already
  if(!nodeHasMoved(startNode)) {
    startNode = getNode({
      isEmpty: false,
      isRoot: false,
      isPiece: true,
      moveRuleCount: 0,
      childIndexOffset: 0,
      parentIndexOffset: nodeParentIndexOffset(startNode),
      piece: getPieceInfo(nodePiece(startNode)),
      hasMoved: true
    });
  }
  board = wipePhantomPawns(board);
  board = removeChildNode(board, startPosition);
  board = removeChildNode(board, realEndPosition);
  if(endPieceIsPhantomPawn) {
    board = removeChildNode(board, realOppositeForwardPosition);
  }
  board = insertNode(board, startNode, endPosition);
  //Create phantompawn if needed
  if(spawnPhantomPawn) {
    board = insertNode(board, getNode({
      isEmpty: false,
      isRoot: false,
      isPiece: true,
      moveRuleCount: 0,
      childIndexOffset: 0,
      parentIndexOffset: 0,
      piece: {
        pieceType: 'phantompawn',
        isWhite: playerIsWhite
      },
      hasMoved: true
    }), forwardPosition);
  }
  board = mergeNodes(board);
  //Execute additional rook move if needed
  if(leftCastling) {
    let leftRookPosition = playerIsWhite ? getPosition({
      recursionLevel: 2,
      recursionCoordinate: 0b000000
    }) : getPosition({
      recursionLevel: 2,
      recursionCoordinate: 0b101010
    });
    let leftRookNode = getNodeFromPosition(board, leftRookPosition);
    let leftRookPiece = nodePiece(leftRookNode);
    board = executeMove(board, createMove(leftRookPosition, leftStepPosition, leftRookPiece));
  }
  if(rightCastling) {
    let rightRookPosition = playerIsWhite ? getPosition({
      recursionLevel: 2,
      recursionCoordinate: 0b010101
    }) : getPosition({
      recursionLevel: 2,
      recursionCoordinate: 0b111111
    });
    let rightRookNode = getNodeFromPosition(board, rightRookPosition);
    let rightRookPiece = nodePiece(rightRookNode);
    board = executeMove(board, createMove(rightRookPosition, rightStepPosition, rightRookPiece));
  }
  //Execute move-rule board modification
  if(isCapture || isPawnMove) {
    board[0] = setNodeMoveRuleCount(board[0], 0);
  }
  else {
    board[0] = setNodeMoveRuleCount(board[0], nodeMoveRuleCount(board[0]));
  }
  return board;
}

export function generateMoves(board: Board, isWhite: boolean, splitMoves: boolean = true, skipCastlingCheck: boolean = false): Move[] {
  let pieceIndexList = [];
  for(let i = 0;i < board.length;i++) {
    if(nodeIsPiece(board[i]) && pieceIsWhite(nodePiece(board[i])) == isWhite) {
      pieceIndexList.push(i);
    }
  }
  let moveList: Move[] = [];
  for(let i = 0;i < pieceIndexList.length;i++) {
    let currPosition = getPositionFromIndex(board, <u32>pieceIndexList[i]);
    let currMoveList = generatePieceMoves(board, currPosition, skipCastlingCheck);
    for(let j = 0;j < currMoveList.length;j++) {
      moveList.push(currMoveList[j]);
    }
  }
  if(splitMoves) {
    let tmpBoard = board.slice(0);
    for(let i = 0;i < pieceIndexList.length;i++) {
      let currPosition = getPositionFromIndex(board, <u32>pieceIndexList[i]);
      tmpBoard = splitNode(tmpBoard, currPosition);
    }
    let currMoveList = generateMoves(tmpBoard, isWhite, false, skipCastlingCheck);
    for(let j = 0;j < currMoveList.length;j++) {
      moveList.push(currMoveList[j]);
    }
  }
  return moveList;
}

export function hasValidMoves(board: Board, isWhite: boolean): boolean {
  let moves = generateMoves(board.slice(0), isWhite);
  for(let i = 0;i < moves.length;i++) {
    let currMove = moves[i];
    if(isMoveValid(board, currMove, isWhite)) {
      return true;
    }
  }
  return false;
}

export function generateValidMoves(board: Board, isWhite: boolean): Move[] {
  let moves = generateMoves(board.slice(0), isWhite);
  let validMoves: Move[] = [];
  for(let i = 0;i < moves.length;i++) {
    let currMove = moves[i];
    if(isMoveValid(board, currMove, isWhite)) {
      validMoves.push(currMove.slice(0));
    }
  }
  return validMoves;
}