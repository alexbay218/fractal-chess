import fetch from 'cross-fetch';
import * as FCEAssembly from './assembly';
import { instantiate } from 'as-loader/runtime/bind';
import { GameState, MoveInfo } from './types';
import { getGameStateInfo, getMove, getMoveInfo } from './assembly/convert';
import { fractalChessGame } from './game';

const isNode = require('is-node');
const fs = require('fs');
const path = require('path');

export default class fractalChessEngine {
  public static fractalChessGame = fractalChessGame;
  gameState: GameState = [[], [], []];
  private loaded: boolean = false;
  private FCE: typeof FCEAssembly;
  private enableWasm: boolean;
  constructor(wasm: boolean = true, onLoad: Function = () => {}) {
    this.enableWasm = wasm;
    (async () => {
      if(!this.FCE) {
        if(!this.enableWasm) {
          this.FCE = (await (await import('./assembly') as any).fallback());
        }
        else {
          const module = await instantiate(FCEAssembly, async (url) => {
            if(isNode) {
              const wasmPath = path.join(__dirname, url.toString());
              if(fs.existsSync(wasmPath)) {
                return await fs.promises.readFile(wasmPath);
              }
            }
            return (await fetch(url)).arrayBuffer();
          }, undefined, true);
          this.FCE = module.exports;
        }
        this.loaded = true;
        this.reset();
        onLoad();
      }
    })();
  }
  isLoaded(): boolean {
    return this.loaded;
  }
  reset() {
    this.gameState = this.FCE.initializeGameState();
  }
  move(move: MoveInfo) {
    this.gameState = this.FCE.executePlayerMove(this.gameState, getMove(move));
  }
  moves() {
    let moves = this.FCE.generatePlayerMoves(this.gameState);
    return moves.map(e => getMoveInfo(e));
  }
  getGameState() {
    return getGameStateInfo(this.gameState);
  }
}