const path = require('path');
const deepcopy = require('deepcopy');

const nodeConfig = {
  mode: 'development',
  target: 'node',
  entry: path.resolve(__dirname, 'src/index.ts'),
  output: {
    filename: 'fractal-chess-engine.node.js',
    path: path.resolve(__dirname, 'build'),
    library: {
      type: 'commonjs-module',
      export: 'default'
    },
    publicPath: ''
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        include: path.resolve(__dirname, 'src/assembly/index.ts'),
        use: [
          {
            loader: 'ts-loader',
            options: {
              transpileOnly: true
            }
          },
          {
            loader: 'as-loader',
            options: {
              name: 'engine.wasm',
              bind: true,
              fallback: true,
              optimizeLevel: 3,
              shrinkLevel: 1,
              coverage: true,
              exportRuntime: true
            }
          }
        ]
      },
      {
        test: /\.ts$/,
        loader: 'ts-loader',
      },
    ],
  },
  watchOptions: {
    ignored: 'node_modules/'
  }
};

const webConfig = deepcopy(nodeConfig);
webConfig.target = 'web';
webConfig.resolve.fallback = {
  fs: false,
  path: false
};
webConfig.output.filename = 'fractal-chess-engine.web.js';
webConfig.output.library.name = 'fractalChessEngine';
webConfig.output.library.type = 'var';
webConfig.output.publicPath = 'auto';

module.exports = [nodeConfig, webConfig];