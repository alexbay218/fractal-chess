# Design

## Overview

 - Fast and efficient computation of checkmate and other functions (for bot purposes)
 - Utilize AssemblyScript and GPU.js where possible
 - Implement the game of Fractal Chess as defined below
 - Implement a bot / bot framework
 - Implement both local and online play

## Game Definition

### Overview

Fractal Chess follows the same rules as regular chess (including castling and en passant). Where Fractal Chess differs is the available moves. As part of any move, any piece can subdivide into four small pieces that operate on a smaller scale (the move then operates on one of the four smaller pieces).

```
┌───┬───┬───┐     ┌───┬───┬─┬─┐
│   │***│   │     │   │***│*│B│
│   │***│   │     │   │***├─┼─┤
│   │***│   │     │   │***│ │*│
├───┼───┼───┤     ├───┼─┬─┼─┴─┤
│***│   │***│     │***│B│ │***│
│***│ B │***│ ==> │***├─┼─┤***│
│***│   │***│     │***│B│B│***│
├───┼───┼───┤     ├───┼─┴─┼───┤
│   │***│   │     │   │***│   │
│   │***│   │     │   │***│   │
│   │***│   │     │   │***│   │
└───┴───┴───┘     └───┴───┴───┘
```

As a result, most of the definition focuses on different sized piece interactions.

### Moving

Smaller pieces move along the smaller grid using the same move rules as their normal chess counterpart. Moves are block by the same colored pieces if the target square contains a same color piece (regardless of size).

### Capturing

Capturing occurs when a move (not block by same color pieces) has a target square that intersects with an opposite color piece. This means smaller pieces can capture large pieces and large pieces can capture smaller pieces (multiple at the same time).

### Dividing and Merging

When subdividing, the piece can only subdivide one level at a time (i.e. one piece turns into four). If a move allows for pieces to recombine into a larger piece, they will do so.

### En Passant

Notable exception to the capturing rule requiring intersecting pieces is en passant. For en passant interactions, imagine a "phantom" pawn that spawns in the square between start and target squares (during a double move). This "phantom" pawn can only be captured by an opponent pawn and only lasts for one turn (during the opponent's turn). On capture, this "phantom" pawn kills the corresponding real pawn.

### Castling

Castling can occur on smaller pieces as well (realistically, this can only occur once). Both pieces must be at the same level (the rook can subdivide to match the same level if needed). Same rules as normal chess also apply.

### Checks

A player is in check when any king can be captured by the opponent. Legal moves must resolve checks.

### Checkmate

Checkmate occurs when no legal moves are available and the player is in check.

### Stalemate

Stalemate occurs when no legal moves are available and the player is not in check.

### Fifty move and Threefold repetition

Both rules still apply.

## Design

To more efficiently store and process data, `u32` and `u16` integers are used to represent the board, pieces, and moves. Even though the definition allows for infinite recursion levels, this design is limited to a depth of 13 (playing on a 8192 x 8192 board with 67 million squares!).

*Note: `LSB0` is used as the bit numbering scheme below (MSB first and LSB is the 0 bit)*

### Schema

#### Position

A `u32` integer representing any position on the board.

 - Bits [0-3] - Indicates level of recursion to represent (starting pieces use level 2).
 - Bits [4-29] - Recursive coordinate of the position. In each level pair, 00 is bottom left, 01 is bottom right, 10 is top left, and 11 is top right (from the white player's perspective).
 - Bits [30-31] - Reserved bits used for promotion in `Move` integer.

```
Example for d6 square:

     ┌ Unused (Changes based on level)
    ┌┴─────────────────┐
  00000000000000000000001101100010
  └┤                    └┬───┘└┬─┘
   └ Reserved            │     └ Recursion Level
                         └ Recursive Coordinate

Recursion Level 0: 10

┌───────────┬───────────┐
│***********│           │
│***********│           │
│***********│           │
│***********│           │
│***********│           │
│***********│           │
│***********│           │
├───────────┼───────────┤
│           │           │
│           │           │
│           │           │
│           │           │
│           │           │
│           │           │
│           │           │
└───────────┴───────────┘

Recursion Level 1: 01

┌─────┬─────┬─────┬─────┐
│     │     │     │     │
│     │     │     │     │
│     │     │     │     │
├─────┼─────┼─────┼─────┤
│     │*****│     │     │
│     │*****│     │     │
│     │*****│     │     │
├─────┼─────┼─────┼─────┤
│     │     │     │     │
│     │     │     │     │
│     │     │     │     │
├─────┼─────┼─────┼─────┤
│     │     │     │     │
│     │     │     │     │
│     │     │     │     │
└─────┴─────┴─────┴─────┘

Recursion Level 2: 11

┌──┬──┬──┬──┬──┬──┬──┬──┐
│  │  │  │  │  │  │  │  │
├──┼──┼──┼──┼──┼──┼──┼──┤
│  │  │  │  │  │  │  │  │
├──┼──┼──┼──┼──┼──┼──┼──┤
│  │  │  │**│  │  │  │  │
├──┼──┼──┼──┼──┼──┼──┼──┤
│  │  │  │  │  │  │  │  │
├──┼──┼──┼──┼──┼──┼──┼──┤
│  │  │  │  │  │  │  │  │
├──┼──┼──┼──┼──┼──┼──┼──┤
│  │  │  │  │  │  │  │  │
├──┼──┼──┼──┼──┼──┼──┼──┤
│  │  │  │  │  │  │  │  │
├──┼──┼──┼──┼──┼──┼──┼──┤
│  │  │  │  │  │  │  │  │
└──┴──┴──┴──┴──┴──┴──┴──┘

Composite View:

┌─────┬─────┬───────────┐
│     │     │           │
│     │     │           │
│     │     │           │
├─────┼──┬──┤           │
│     │  │**│           │
│     ├──┼──┤           │
│     │  │  │           │
├─────┴──┴──┼───────────┤
│           │           │
│           │           │
│           │           │
│           │           │
│           │           │
│           │           │
│           │           │
└───────────┴───────────┘
```

#### Direction

A `u4` integer representing a cardinal direction.

```
   0
   ^
   |
3<-+->1
   |
   v
   2
```

#### Piece

A `u4` integer representing a piece in piece color and type.

 - Bit [0] - Indicate piece color (white is `0` and black is `1`).
 - Bits [1-3] - Indicate piece type.
   - 0 - Pawn
   - 1 - Knight
   - 2 - Bishop
   - 3 - Rook
   - 4 - Queen
   - 5 - King
   - 6 - Phantom Pawn (used for en passant)

*Note: lowest available integer type in TypeScript and AssemblyScript is `u8`, so internally `u8` is used, but only the first 4 bits actually mean anything.*

#### Move

A `u32` integer array representing start and target positions to represent a move.

 - Move[0] Bits [0-29] - `Position` integer representing start position (shrunk down to 30 bits by removing reserved bits).
 - Move[1] Bits [0-29] - `Position` integer representing target position (shrunk down to 30 bits by removing reserved bits).
 - Move[0] Bits [30-31] + Move[1] Bits [30-31] - `Piece` integer representing promotion (MSB is Move[1] Bit 31, LSB is Move[0] Bit 30).

#### Move History

A `u32` integer array representing the move history of a game. Each move is two elements.

#### Node

A `u32` integer used as a node for the quad-tree that represents the board. Note, certain portions may constitute a signed integer (instead of unsigned integer).

 - Bit [0] - Indicates if node is empty or not (`0` for empty and `1` for not empty).

If node is empty

 - Bit [1] - Indicates if node is a root node or not (`0` for non-root and `1` for root node).

> If node is not a root node
> 
> - Bits [2-16] - Unused (reserved for future use).
>  - Bits [17-31] - Parent index offset (i14). In linear quad-tree, the parent normally have an index of `((child index) - 1) / 4`. Since parents have a children index offset, this value is used to adjust children index to accurately calculate parent index. The new equation for parent index is `((child index) - 1) / 4 + (parent index offset)`.
> 
> If node is a root node
> 
> - Bits [2-9] - Indicate the number of moves since last pawn or capture moves. *Note: the fifty move rule counts player and opponent moves as one "move", so this number needs to 100 before draw request can be made. Draw is automatic when this number reaches 150.*
> - Bits [9-31] - Unused (reserved for future use).

If node is not empty

 - Bit [1] - Indicates if node is a piece node or not (`0` for non-piece and `1` for piece node).

> If node is not a piece node
> 
>  - Bits [2-16] - Children index offset (i14). In linear quad-tree, the children normally have an starting index of `((parent index) * 4) + 1`. Since this often causes holes in the array, this value is used to adjust children index to remove holes. The new equation for starting children index is `((parent index) * 4) + 1 - (child index offset)`.
>  - Bits [17-31] - Parent index offset (i14). In linear quad-tree, the parent normally have an index of `((child index) - 1) / 4`. Since parents have a children index offset, this value is used to adjust children index to accurately calculate parent index. The new equation for parent index is `((child index) - 1) / 4 + (parent index offset)`.
> 
> If node is a piece node
> 
>  - Bits [2-5] - `Piece` integer of the piece type.
>  - Bit [6] - Indicates if piece has moved (unmoved is `0` and moved is `1`).
>  - Bits [7-16] - Unused (reserved for future use).
>  - Bits [17-31] - Parent index offset (i14). In linear quad-tree, the parent normally have an index of `((child index) - 1) / 4`. Since parents have a children index offset, this value is used to adjust children index to accurately calculate parent index. The new equation for parent index is `((child index) - 1) / 4 + (parent index offset)`.

#### Board

A `u32` integer array representing the game board. This is structured as a compressed linear quad-tree structure. Since quad-tree location gives the inherit position and nodes that contain children do not need to store piece data, extra information regarding parent and child index offset to remove "holes" in the array. Children are laid out in the same order as the `Position` integer's recursive coordinate pairs (00 is bottom left, 01 is bottom right, 10 is top left, and 11 is top right).

```
Example:
        A
   ┌─┬──┴──┬─┐
   B C     D E
┌─┬┴┬─┐ ┌─┬┴┬─┐
F G H I J K L M

Under normal linear quad-tree indexing:
(child position) = (00 is bottom left, 01 is bottom right, 10 is top left, and 11 is top right)
(child index) = (parent index) * 4 + (child position) + 1
(parent index) = ((child index) - 1) / 4 
[Note: child position is not needed since integer division floors the output]

Resulting node information:
A = { index:  0 }
B = { index:  1 }
C = { index:  2 }
D = { index:  3 }
E = { index:  4 }
F = { index:  5 }
G = { index:  6 }
H = { index:  7 }
I = { index:  8 }
J = { index: 13 }
K = { index: 14 }
L = { index: 15 }
M = { index: 16 }
[Note the "hole" between I and J, compressed linear quad-tree works to remove these holes]

Using compressed linear quad-tree indexing:
(child position) = (00 is bottom left, 01 is bottom right, 10 is top left, and 11 is top right)
(child index) = (parent index) * 4 + (child position) + 1 - (child index offset)
(parent index) = ((child index) - 1) / 4  + (parent index offset)
[Note: child position is not needed since integer division floors the output]

Resulting node information:
A = { index:  0, childOffset: 0, parentOffset: 0 }
B = { index:  1, childOffset: 0, parentOffset: 0 }
C = { index:  2, childOffset: 0, parentOffset: 0 }
D = { index:  3, childOffset: 4, parentOffset: 0 }
E = { index:  4, childOffset: 0, parentOffset: 0 }
F = { index:  5, childOffset: 0, parentOffset: 0 }
G = { index:  6, childOffset: 0, parentOffset: 0 }
H = { index:  7, childOffset: 0, parentOffset: 0 }
I = { index:  8, childOffset: 0, parentOffset: 0 }
J = { index:  9, childOffset: 0, parentOffset: 1 }
K = { index: 10, childOffset: 0, parentOffset: 1 }
L = { index: 11, childOffset: 0, parentOffset: 1 }
M = { index: 12, childOffset: 0, parentOffset: 1 }
```

#### Board Hash

A `u32` integer which is the CRC32 hash of a `Board` array. Calculated by concatenating the `Board` array into a binary blob and running CRC32 on it. Bits 30 and 31 will be truncated.

#### Board Hash Count

A `u32` integer which contains how many board during the game have the same `Board Hash`.

 - Bits [0-1] - Integer represent the count.
 - Bits [2-31] - `Board Hash` representing the board associated with the count.

#### Board Hash Counts

A `u32` integer array containing all the `Board Hash Count` accumulated during the game. Used for threefold repetition calculations.