const { Server, Origins } = require('boardgame.io/server');
const { fractalChessGame } = require('../../engine/src/game');

const server = Server({
  games: [fractalChessGame],
  origins: [
    '*',
    'http://alexbay218.gitlab.io/fractal-chess/',
    'https://fractal-chess.netlify.app',
    Origins.LOCALHOST
  ],
});

server.run(8001);
