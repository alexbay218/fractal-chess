document.getElementById('tutorialContent').innerHTML = `<div class="container-md">
  <h5>Overview</h5>
  <p>
    Fractal Chess follows the same rules as regular chess (including castling and en passant).
    Where Fractal Chess differs is the available moves.
    As part of any move, any piece can subdivide into four small pieces that operate on a smaller scale (the move then operates on one of the four smaller pieces).
  </p>
  <pre style="line-height: 80%; padding: 5px; text-align: center;">┌───┬───┬───┐     ┌───┬───┬─┬─┐
│   │███│   │     │   │███│█│B│
│   │███│   │     │   │███├─┼─┤
│   │███│   │     │   │███│ │█│
├───┼───┼───┤     ├───┼─┬─┼─┴─┤
│███│   │███│     │███│B│ │███│
│███│ B │███│ ==> │███├─┼─┤███│
│███│   │███│     │███│B│B│███│
├───┼───┼───┤     ├───┼─┴─┼───┤
│   │███│   │     │   │███│   │
│   │███│   │     │   │███│   │
│   │███│   │     │   │███│   │
└───┴───┴───┘     └───┴───┴───┘</pre>
  <p>
    As a result, most of the definition focuses on different sized piece interactions.
  </p>
  <h5>Moving</h5>
  <p>
    Smaller pieces move along the smaller grid using the same move rules as their normal chess counterpart.
    Moves are block by the same colored pieces if the target square contains a same color piece (regardless of size).
  </p>
  <h5>Capturing</h5>
  <p>
    Capturing occurs when a move (not block by same color pieces) has a target square that intersects with an opposite color piece.
    This means smaller pieces can capture large pieces and large pieces can capture smaller pieces (multiple at the same time).
  </p>
  <h5>Dividing and Merging</h5>
  <p>
    When subdividing, the piece can only subdivide one level at a time (i.e. one piece turns into four).
    If a move allows for pieces to recombine into a larger piece, they will do so.\
    Pieces will only recombine along the higher level square (see Recursion Level Example below).
  </p>
  <h5>En Passant</h5>
  <p>
    Notable exception to the capturing rule requiring intersecting pieces is en passant.
    For en passant interactions, imagine a "phantom" pawn that spawns in the square between start and target squares (during a double move).
    This "phantom" pawn can only be captured by an opponent pawn and only lasts for one turn (during the opponent's turn).
    On capture, this "phantom" pawn kills the corresponding real pawn.
  </p>
  <h5>Castling</h5>
  <p>
    Castling can occur on smaller pieces as well (realistically, this can only occur once).
    Both pieces must be at the same level (the rook can subdivide to match the same level if needed).
    Same rules as normal chess also apply.
  </p>
  <h5>Checks</h5>
  <p>
    A player is in check when any king can be captured by the opponent.
    Legal moves must resolve checks.
  </p>
  <h5>Checkmate</h5>
  <p>
    Checkmate occurs when no legal moves are available and the player is in check.
  </p>
  <h5>Stalemate</h5>
  <p>
    Stalemate occurs when no legal moves are available and the player is not in check.
  </p>
  <h5>Fifty move and Threefold repetition</h5>
  <p>
    Both rules still apply.
  </p>
  <h5>Recursion Level Example</h5>
  <p>
    The diagrams below show how recursion levels work in partitioning the board (and how it affects merging).
  </p>
  <pre style="line-height: 80%; padding: 5px; text-align: center;">Recursion Level 0
┌───────────┬───────────┐
│███████████│           │
│███████████│           │
│███████████│           │
│███████████│           │
│███████████│           │
│███████████│           │
│███████████│           │
├───────────┼───────────┤
│           │           │
│           │           │
│           │           │
│           │           │
│           │           │
│           │           │
│           │           │
└───────────┴───────────┘

Recursion Level 1

┌─────┬─────┬─────┬─────┐
│     │     │     │     │
│     │     │     │     │
│     │     │     │     │
├─────┼─────┼─────┼─────┤
│     │█████│     │     │
│     │█████│     │     │
│     │█████│     │     │
├─────┼─────┼─────┼─────┤
│     │     │     │     │
│     │     │     │     │
│     │     │     │     │
├─────┼─────┼─────┼─────┤
│     │     │     │     │
│     │     │     │     │
│     │     │     │     │
└─────┴─────┴─────┴─────┘

Recursion Level 2

┌──┬──┬──┬──┬──┬──┬──┬──┐
│  │  │  │  │  │  │  │  │
├──┼──┼──┼──┼──┼──┼──┼──┤
│  │  │  │  │  │  │  │  │
├──┼──┼──┼──┼──┼──┼──┼──┤
│  │  │  │██│  │  │  │  │
├──┼──┼──┼──┼──┼──┼──┼──┤
│  │  │  │  │  │  │  │  │
├──┼──┼──┼──┼──┼──┼──┼──┤
│  │  │  │  │  │  │  │  │
├──┼──┼──┼──┼──┼──┼──┼──┤
│  │  │  │  │  │  │  │  │
├──┼──┼──┼──┼──┼──┼──┼──┤
│  │  │  │  │  │  │  │  │
├──┼──┼──┼──┼──┼──┼──┼──┤
│  │  │  │  │  │  │  │  │
└──┴──┴──┴──┴──┴──┴──┴──┘

Composite View:

┌─────┬─────┬───────────┐
│     │     │           │
│     │     │           │
│     │     │           │
├─────┼──┬──┤           │
│     │  │██│           │
│     ├──┼──┤           │
│     │  │  │           │
├─────┴──┴──┼───────────┤
│           │           │
│           │           │
│           │           │
│           │           │
│           │           │
│           │           │
│           │           │
└───────────┴───────────┘</pre>
</div>`;