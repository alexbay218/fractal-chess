import { BoardInfo, MoveInfo, PieceInfo, PositionInfo } from '../../engine/src/types';
import { getPositionFromIndex } from '../../engine/src/assembly/board';
import { getBoard, getPositionInfo } from '../../engine/src/assembly/convert';
import canvg, { Canvg, Parser } from 'canvg';
import bB from 'data-url:./assets/bB.svg';
import bK from 'data-url:./assets/bK.svg';
import bN from 'data-url:./assets/bN.svg';
import bP from 'data-url:./assets/bP.svg';
import bQ from 'data-url:./assets/bQ.svg';
import bR from 'data-url:./assets/bR.svg';
import wB from 'data-url:./assets/wB.svg';
import wK from 'data-url:./assets/wK.svg';
import wN from 'data-url:./assets/wN.svg';
import wP from 'data-url:./assets/wP.svg';
import wQ from 'data-url:./assets/wQ.svg';
import wR from 'data-url:./assets/wR.svg';
import { boardCoordinateFromPosition, screenCoordinateFromBoardCoordinate, SquareCoordinates, ZoomState } from './zoom';
const parser = new Parser();
const parsedBB = parser.parseFromString(decodeURIComponent(bB.replace('data:image/svg+xml,','')));
const parsedBK = parser.parseFromString(decodeURIComponent(bK.replace('data:image/svg+xml,','')));
const parsedBN = parser.parseFromString(decodeURIComponent(bN.replace('data:image/svg+xml,','')));
const parsedBP = parser.parseFromString(decodeURIComponent(bP.replace('data:image/svg+xml,','')));
const parsedBQ = parser.parseFromString(decodeURIComponent(bQ.replace('data:image/svg+xml,','')));
const parsedBR = parser.parseFromString(decodeURIComponent(bR.replace('data:image/svg+xml,','')));
const parsedWB = parser.parseFromString(decodeURIComponent(wB.replace('data:image/svg+xml,','')));
const parsedWK = parser.parseFromString(decodeURIComponent(wK.replace('data:image/svg+xml,','')));
const parsedWN = parser.parseFromString(decodeURIComponent(wN.replace('data:image/svg+xml,','')));
const parsedWP = parser.parseFromString(decodeURIComponent(wP.replace('data:image/svg+xml,','')));
const parsedWQ = parser.parseFromString(decodeURIComponent(wQ.replace('data:image/svg+xml,','')));
const parsedWR = parser.parseFromString(decodeURIComponent(wR.replace('data:image/svg+xml,','')));

export function renderBoard(ctx: CanvasRenderingContext2D, zoomState: ZoomState, board: BoardInfo) {
  //Clear canvas
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  //Draw border
  let boardCoords = boardCoordinateFromPosition({ recursionLevel: -1, recursionCoordinate: 0b10 });
  boardCoords.x -= Math.pow(2, 47);
  boardCoords.y -= Math.pow(2, 47);
  boardCoords.width += Math.pow(2, 48);
  boardCoords.height += Math.pow(2, 48);
  let coords = screenCoordinateFromBoardCoordinate(zoomState, boardCoords);
  ctx.fillStyle = '#000000';
  ctx.fillRect(coords.x, coords.y, coords.width, coords.height);
  //Draw required squares
  let squareCoords = boardCoordinateFromPosition({ recursionLevel: 2, recursionCoordinate: 0b101010 });
  for(let x = 0;x < 8;x++) {
    for(let y = 0;y < 8;y++) {
      let coords = screenCoordinateFromBoardCoordinate(zoomState, squareCoords);
      renderSquare(ctx, x % 2 === y % 2, {
        x: coords.x + (x * coords.height),
        y: coords.y + (y * coords.width),
        height: coords.height,
        width: coords.width
      });
    }
  }
  for(let i = 0;i < board.length;i++) {
    if(board[i].isPiece || board[i].isEmpty) {
      let pos = getPositionInfo(getPositionFromIndex(getBoard(board), i));
      if(pos.recursionLevel > 2) {
        //Draw squares
        let coords = screenCoordinateFromBoardCoordinate(zoomState, boardCoordinateFromPosition(pos));
        let lastRecursionCoordinate = (pos.recursionCoordinate >> (2 * pos.recursionLevel)) & 0x3;
        let isWhite = lastRecursionCoordinate === 1 || lastRecursionCoordinate === 2;
        renderSquare(ctx, isWhite, coords);
        //Draw square outlines
        coords = screenCoordinateFromBoardCoordinate(zoomState, boardCoordinateFromPosition({
          recursionCoordinate: pos.recursionCoordinate,
          recursionLevel: pos.recursionLevel - 1
        }));
        renderOutline(ctx, coords);
      }
    }
  }
  //Draw pieces
  for(let i = 0;i < board.length;i++) {
    if(board[i].isPiece) {
      let pos = getPositionInfo(getPositionFromIndex(getBoard(board), i));
      let coords = screenCoordinateFromBoardCoordinate(zoomState, boardCoordinateFromPosition(pos));
      renderPiece(ctx, board[i].piece, coords);
    }
  }
}

function renderPiece(ctx: CanvasRenderingContext2D, piece: PieceInfo, coordinate: SquareCoordinates) {
  let selectedPiece = null;
  if(piece.isWhite) {
    if(piece.pieceType === 'pawn') { selectedPiece = parsedWP; }
    if(piece.pieceType === 'bishop') { selectedPiece = parsedWB; }
    if(piece.pieceType === 'knight') { selectedPiece = parsedWN; }
    if(piece.pieceType === 'rook') { selectedPiece = parsedWR; }
    if(piece.pieceType === 'queen') { selectedPiece = parsedWQ; }
    if(piece.pieceType === 'king') { selectedPiece = parsedWK; }
  }
  else {
    if(piece.pieceType === 'pawn') { selectedPiece = parsedBP; }
    if(piece.pieceType === 'bishop') { selectedPiece = parsedBB; }
    if(piece.pieceType === 'knight') { selectedPiece = parsedBN; }
    if(piece.pieceType === 'rook') { selectedPiece = parsedBR; }
    if(piece.pieceType === 'queen') { selectedPiece = parsedBQ; }
    if(piece.pieceType === 'king') { selectedPiece = parsedBK; }
  }
  if(selectedPiece !== null) {
    if(coordinate.width > 3 && coordinate.height > 3) {
      ctx.fillStyle = '#000000';
      let drawnPiece = new Canvg(ctx, selectedPiece, {
        enableRedraw: false,
        ignoreAnimation: true,
        ignoreClear: true,
        ignoreDimensions: true,
        ignoreMouse: true,
        offsetX: (coordinate.x + ((45 * ((coordinate.width / 45) - 1)) / 2)) * (45 / coordinate.width),
        offsetY: (coordinate.y + ((45 * ((coordinate.height / 45) - 1)) / 2)) * (45 / coordinate.height),
        scaleWidth: coordinate.width,
        scaleHeight: coordinate.height,
      });
      drawnPiece.render();
    }
    else if(coordinate.width > 1 && coordinate.height > 1) {
      ctx.fillStyle = piece.isWhite ? '#ffffff' : '#000000';
      ctx.beginPath();
      ctx.arc(coordinate.x + (coordinate.width / 2), coordinate.y + (coordinate.height / 2), coordinate.width / 4, 0, 2 * Math.PI);
      ctx.fill();
    }
  }
}

function renderSquare(ctx: CanvasRenderingContext2D, isWhite: boolean, coordinate: SquareCoordinates) {
  if(isWhite) {
    ctx.fillStyle = '#dddddd';
  }
  else {
    ctx.fillStyle = '#444444';
  }
  if(coordinate.width > 1 && coordinate.height > 1) {
    ctx.fillRect(coordinate.x, coordinate.y, coordinate.width, coordinate.height);
  }
}

function renderOutline(ctx: CanvasRenderingContext2D, coordinate: SquareCoordinates) {
  ctx.strokeStyle = '#808080';
  let newLineWidth = Math.min(coordinate.width, coordinate.height) / 32;
  ctx.lineWidth = newLineWidth;
  if(coordinate.width > 1 && coordinate.height > 1) {
    ctx.strokeRect(
      coordinate.x,
      coordinate.y,
      coordinate.width,
      coordinate.height
    );
  }
}

export function renderMove(ctx: CanvasRenderingContext2D, zoomState: ZoomState, move: MoveInfo, renderSource: boolean = true) {
  let startCoords = screenCoordinateFromBoardCoordinate(zoomState, boardCoordinateFromPosition(move.startingPosition));
  if(renderSource) {
    renderHighlight(ctx, 'source', startCoords);
  }
  let endCoords = screenCoordinateFromBoardCoordinate(zoomState, boardCoordinateFromPosition(move.endingPosition));
  renderHighlight(ctx, 'move', endCoords);
}

export function renderHighlight(ctx: CanvasRenderingContext2D, type: 'source' | 'move' | 'capture', coordinate: SquareCoordinates) {
  if(type === 'source') {
    ctx.fillStyle = '#0000ff';
  }
  if(type === 'move') {
    ctx.fillStyle = '#00ff00';
  }
  ctx.globalAlpha = 0.30;
  if(coordinate.width > 1 && coordinate.height > 1) {
    ctx.fillRect(coordinate.x, coordinate.y, coordinate.width, coordinate.height);
  }
  ctx.globalAlpha = 1;
}