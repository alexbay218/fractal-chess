import { getPositionFromIndex } from '../../engine/src/assembly/board';
import { getBoard, getPositionInfo } from '../../engine/src/assembly/convert';
import { BoardInfo, PositionInfo } from '../../engine/src/types';


//Board size is 2^52 by 2^52 pixels
const maxBoardSide = Math.pow(2, 52);
export interface SquareCoordinates {
  x: number
  y: number
  width: number
  height: number
};

export interface ZoomState {
  offsetX: number
  offsetY: number
  scale: number
}

export function initializeZoomState(width, height): ZoomState {
  let scale = Math.min(width, height) * 0.9;
  return {
    offsetX: (width / 2) - (maxBoardSide / 2 * (scale / maxBoardSide)),
    offsetY: (height / 2) - (maxBoardSide / 2 * (scale / maxBoardSide)),
    scale: scale
  };
}

export function pan(zoomState: ZoomState, mouseDX: number, mouseDY: number): ZoomState {
  zoomState.offsetX += mouseDX;
  zoomState.offsetY += mouseDY;
  return zoomState;
}

export function zoom(zoomState: ZoomState, mouseX: number, mouseY: number, wheelDY: number): ZoomState {
  let deltaScale = wheelDY > 0 ? -1 : 1;
  let newScale = zoomState.scale + (deltaScale / (10 / zoomState.scale));
  zoomState.offsetX += (mouseX - zoomState.offsetX) - ((mouseX - zoomState.offsetX) * (newScale / zoomState.scale));
  zoomState.offsetY += (mouseY - zoomState.offsetY) - ((mouseY - zoomState.offsetY) * (newScale / zoomState.scale));
  zoomState.scale = newScale;
  return zoomState
}

export function boardCoordinateFromPosition(position: PositionInfo): SquareCoordinates {
  let res = {
    x: 0,
    y: 0,
    width: 0,
    height: 0
  };
  res.width = Math.round(maxBoardSide / Math.pow(2, position.recursionLevel + 1));
  res.height = Math.round(maxBoardSide / Math.pow(2, position.recursionLevel + 1));
  for(let l = 0;l <= position.recursionLevel;l++) {
    const currentRecursionCoordinate = (position.recursionCoordinate >> (2 * l)) & 0x3;
    const left = currentRecursionCoordinate & 0x1;
    const bottom = 1 - ((currentRecursionCoordinate >> 1) & 0x1);
    res.x += (maxBoardSide / Math.pow(2, l + 1)) * left;
    res.y += (maxBoardSide / Math.pow(2, l + 1)) * bottom;
  }
  return res;
}

export function screenCoordinateFromBoardCoordinate(zoomState: ZoomState, boardCoordinate: SquareCoordinates): SquareCoordinates {
  let res = {
    x: boardCoordinate.x * (zoomState.scale / maxBoardSide) + zoomState.offsetX,
    y: boardCoordinate.y * (zoomState.scale / maxBoardSide) + zoomState.offsetY,
    width: boardCoordinate.width * (zoomState.scale / maxBoardSide),
    height: boardCoordinate.height * (zoomState.scale / maxBoardSide)
  };
  return res;
}

export function positionFromMouse(zoomState: ZoomState, mouseX: number, mouseY: number, offsetY: number = 0): PositionInfo {
  let boardX = (mouseX - zoomState.offsetX) / (zoomState.scale / maxBoardSide);
  let boardY = ((mouseY - offsetY) - zoomState.offsetY) / (zoomState.scale / maxBoardSide);
  let res = {
    recursionLevel: 12,
    recursionCoordinate: 0
  };
  let levelAccumX = 0;
  let levelAccumY = 0;
  for(let l = 0;l < 13;l++) {
    if(boardX > levelAccumX + (maxBoardSide / Math.pow(2, l + 1))) {
      res.recursionCoordinate += 1 << (l * 2);
      levelAccumX += (maxBoardSide / Math.pow(2, l + 1));
    }
    if(boardY < levelAccumY + (maxBoardSide / Math.pow(2, l + 1))) {
      res.recursionCoordinate += 2 << (l * 2);
    }
    else {
      levelAccumY += (maxBoardSide / Math.pow(2, l + 1));
    }
  }
  return res;
}
