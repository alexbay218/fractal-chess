import { LobbyClient } from 'boardgame.io/client';

(window as any).serverUrl = 'http://35.202.152.170:8001';
(window as any).matchID = null;
(window as any).playerID = null;
(window as any).playerCred = null;
(window as any).playerName = '';
//@ts-expect-error
const gameName = fractalChessEngine.fractalChessGame.name;

const lobbyClient = new LobbyClient({ server: (window as any).serverUrl });
const lobbyList = document.getElementById('lobbyList');
const lobbyRefresh = async () => {
  const { matches } = await lobbyClient.listMatches(gameName);
  lobbyList.innerHTML = '';
  for(let match of matches) {
    lobbyList.innerHTML += `<div class="row">
  <div class="col-8">
    ${match.players[0].name ? match.players[0].name : `<em>[Empty Slot]</em>`}
    vs
    ${match.players[1].name ? match.players[1].name : `<em>[Empty Slot]</em>`}
  </div>
  <div class="col-4">
    <button class="btn btn-outline-secondary w-100" type="button" onclick="window.joinLobby('${match.matchID}')">Join Game</button>
  </div>
</div>`;
  }
}
document.getElementById('lobbyRefreshButton').addEventListener('click', () => {
  lobbyRefresh();
});
lobbyRefresh();

const newLobby = async () => {
  let playerName = (document.getElementById('lobbyUsernameText') as HTMLInputElement).value;
  if(playerName.length > 0) {
    const { matchID } = await lobbyClient.createMatch(gameName, { numPlayers: 2 });
    console.info(`Creating match ${matchID}`);
    await lobbyRefresh();
  }
  else {
    window.alert('Username Required!');
  }
}
document.getElementById('lobbyNewGameButton').addEventListener('click', () => {
  newLobby();
});

const joinLobby = async (matchID) => {
  let playerName = (document.getElementById('lobbyUsernameText') as HTMLInputElement).value;
  if(playerName.length > 0) {
    console.info(`Joining match ${matchID}`);
    const { playerCredentials, playerID } = await lobbyClient.joinMatch(
      gameName,
      matchID,
      {
        playerName: playerName
      }
    );
    (window as any).matchID = matchID;
    (window as any).playerID = playerID;
    (window as any).playerCred = playerCredentials;
    (window as any).playerName = playerName;
    const triggerEl = document.querySelector('#rootTab button[data-bs-target="#gameContent"]');
    (triggerEl as HTMLElement).click();
  }
  else {
    window.alert('Username Required!');
  }
}
(window as any).joinLobby = joinLobby;