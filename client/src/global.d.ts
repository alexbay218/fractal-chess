declare module 'bundle-text:*' {
  const value: string;
  export default value;
}

declare module 'data-url:*' {
  const value: string;
  export default value;
}

//Needed since library is imported via custom webpack bundle
declare const fractalChessEngine: any;