import { Client } from 'boardgame.io/client';
import { SocketIO, Local } from 'boardgame.io/multiplayer';
import { getPositionFromIndex } from '../../engine/src/assembly/board';
import { getBoard, getPosition } from '../../engine/src/assembly/convert';
import { positionIntersects } from '../../engine/src/assembly/position';
import { MoveInfo } from '../../engine/src/types';
import { fractalChessGame } from '../../engine/src/game';
import { renderBoard, renderHighlight, renderMove } from "./render";
import { boardCoordinateFromPosition, initializeZoomState, pan, positionFromMouse, screenCoordinateFromBoardCoordinate, zoom, ZoomState } from './zoom';

let fce = fractalChessGame;
let zoomState: ZoomState = initializeZoomState(window.innerWidth, window.innerHeight);

let client = Client({
  game: fce,
  multiplayer: Local(),
  debug: false
});
client.start();
let selectedMoves = [];
let canvas: HTMLCanvasElement = document.getElementById('gameCanvas') as HTMLCanvasElement;
let ctx: CanvasRenderingContext2D = canvas.getContext('2d');
let isDragging = false;

let resize = (zoom = false) => {
  let top = canvas.getBoundingClientRect().top;
  let height = window.innerHeight - top - 7;
  canvas.width = window.innerWidth;
  canvas.height = height;
  if(zoom) {
    zoomState = initializeZoomState(window.innerWidth, height);
  }
};
window.addEventListener('resize', () => {
  resize();
});
document.getElementById('gameTab').addEventListener('click', () => {
  client.stop();
  client = Client({
    game: fce,
    multiplayer: SocketIO({
      server: (window as any).serverUrl
    }),
    matchID: (window as any).matchID,
    playerID: (window as any).playerID,
    credentials: (window as any).playerCred,
    debug: false
  });
  client.start();
  client.subscribe((state) => {
    render();
    if(state && state.ctx && state.ctx.gameover) {
      if(state.ctx.gameover.draw) {
        window.alert('Draw!');
      }
      else {
        window.alert((state.ctx.gameover.winner === '0' ? 'White' : 'Black') + ' Wins!');
      }
    }
  });
  (window as any).client = client;
  window.setTimeout(() => {
    resize(true);
  }, 50);
});
resize();

canvas.onmousedown = () => {
  isDragging = false;
}
canvas.onmouseup = (event) => {
  if(!isDragging && client.getState()) {
    let top = canvas.getBoundingClientRect().top;
    let mousePosition = positionFromMouse(zoomState, event.x, event.y, top);
    //Execute move if selected
    let hasMoved = false;
    for(let move of selectedMoves) {
      if(
        !hasMoved &&
        positionIntersects(
          getPosition(move.endingPosition), 
          getPosition(mousePosition)
        )
      ) {
        console.info(`Move piece with ${JSON.stringify(move)}`);
        client.moves.movePiece(JSON.stringify(move));
        hasMoved = true;
      }
    }
    selectedMoves = [];
    //Render moves if selected
    if(client.getState().isActive) {
      let moves = client.getState().G.moves;
      let splitMoves = event.ctrlKey;
      let tmpBoard = getBoard(client.getState().G.gameState.board);
      for(let move of moves) {
        if(positionIntersects(
          getPosition(move.startingPosition),
          getPosition(mousePosition)
        )) {
          let sameLevel = false;
          for(let i = 0;i < tmpBoard.length;i++) {
            if((getPositionFromIndex(tmpBoard, i) === getPosition(move.startingPosition))) {
              sameLevel = true;
            }
          }
          if(splitMoves === !sameLevel) {
            selectedMoves.push(move);
          }
        }
      }
    }
  }
}

canvas.onmousemove = (event) => {
  isDragging = true;
  if((event.buttons & 0x1) === 1) {
    zoomState = pan(zoomState, event.movementX, event.movementY);
  }
}

canvas.onwheel = (event) => {
  zoomState = zoom(zoomState, event.x, event.y,event.deltaY);
}

const render = () => {
  if(client.getState()) {
    renderBoard(ctx, zoomState, client.getState().G.gameState.board);
    let sourceDrawn = false;
    for(let move of selectedMoves) {
      renderMove(ctx, zoomState, move, !sourceDrawn);
      sourceDrawn = true;
    }
  }
}
(window as any).renderLoop = () => {
  window.requestAnimationFrame(() => {
    render();
    (window as any).renderLoop();
  })
}

(window as any).renderLoop();