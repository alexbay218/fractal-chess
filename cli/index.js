const fractalChessEngine = require('../engine/build/fractal-chess-engine.node');
const repl = require('repl');
const r = repl.start('node> ');
r.context.FCE = fractalChessEngine;
r.context.fce = new fractalChessEngine();
